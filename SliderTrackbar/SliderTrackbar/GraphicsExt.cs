﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Text;

namespace System.Drawing
{
    public static class GraphicsExt
    {
        #region Rounded Rectangle http://stackoverflow.com/questions/33853434/how-to-draw-a-rounded-rectangle-in-c-sharp

        /// <summary>
        /// Angles of a rectangle.
        /// </summary>
        public enum RoundAngles
        {
            None = 0,
            TopLeft = 1,
            TopRight = 2,
            BottomLeft = 4,
            BottomRight = 8,
            All = TopLeft | TopRight | BottomLeft | BottomRight
        }

        /// <summary>
        /// Draw, fill a rounded rectangle.
        /// </summary>
        /// <param name="G">The graphics object to use.</param>
        /// <param name="P">The pen to use to draw the rounded rectangle.</param>
        /// <param name="Re">The rectangle to draw.</param>
        /// <param name="Radius">Radius for the rounded angles.</param>
        /// <param name="Angles">Angles to round.</param>
        public static void DrawRoundedRectangle(Graphics G, Pen P, Rectangle Re, int Radius, RoundAngles Angles)
        {
            G.DrawPath(P, RoundedRectanglePath(Re, Radius, Angles));
        }

        /// <summary>
        /// Draw, fill a rounded rectangle.
        /// </summary>
        /// <param name="G">The graphics object to use.</param>
        /// <param name="P">The pen to use to draw the rounded rectangle.</param>
        /// <param name="Re">The rectangle to draw.</param>
        /// <param name="Radius">Radius for the rounded angles.</param>
        /// <param name="Angles">Angles to round.</param>
        public static void DrawRoundedRectangle(Graphics G, Pen P, RectangleF Re, float Radius, RoundAngles Angles)
        {
            G.DrawPath(P, RoundedRectanglePath(Re, Radius, Angles));
        }

        /// <summary>
        /// Fill a rounded rectangle.
        /// </summary>
        /// <param name="G">The graphics object to use.</param>
        /// <param name="B">The brush to fill the rounded rectangle.</param>
        /// <param name="Re">The rectangle to draw.</param>
        /// <param name="Radius">Radius for the rounded angles.</param>
        /// <param name="Angles">Angles to round.</param>
        public static void FillRoundedRectangle(Graphics G, Brush B, Rectangle Re, int Radius, RoundAngles Angles)
        {
            G.FillPath(B, RoundedRectanglePath(Re, Radius, Angles));
        }

        /// <summary>
        /// Fill a rounded rectangle.
        /// </summary>
        /// <param name="G">The graphics object to use.</param>
        /// <param name="B">The brush to fill the rounded rectangle.</param>
        /// <param name="Re">The rectangle to draw.</param>
        /// <param name="Radius">Radius for the rounded angles.</param>
        /// <param name="Angles">Angles to round.</param>
        public static void FillRoundedRectangle(Graphics G, Brush B, RectangleF Re, float Radius, RoundAngles Angles)
        {
            G.FillPath(B, RoundedRectanglePath(Re, Radius, Angles));
        }

        static GraphicsPath RoundedRectanglePath(Rectangle Bounds, int Radius, RoundAngles Angles)
        {
            int Diameter = FixRoundRadius(Radius, Bounds.Width, Bounds.Height) * 2;
            Rectangle arc = new Rectangle(Bounds.Location, new Size(Diameter, Diameter));
            GraphicsPath path = new GraphicsPath();

            if (Diameter == 0)
            {
                path.AddRectangle(Bounds);
                return path;
            }

            if (HasFlag(Angles, RoundAngles.TopLeft))
                path.AddArc(arc, 180, 90);
            else
                path.AddLine(Bounds.Location, Bounds.Location);

            arc.X = Bounds.Right - Diameter;

            if (HasFlag(Angles, RoundAngles.TopRight))
                path.AddArc(arc, 270, 90);
            else
                path.AddLine(Bounds.X + Bounds.Width, Bounds.Y, Bounds.X + Bounds.Width, Bounds.Y);

            arc.Y = Bounds.Bottom - Diameter;

            if (HasFlag(Angles, RoundAngles.BottomRight))
                path.AddArc(arc, 0, 90);
            else
                path.AddLine(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height,
                    Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);

            arc.X = Bounds.Left;

            if (HasFlag(Angles, RoundAngles.BottomLeft))
                path.AddArc(arc, 90, 90);
            else
                path.AddLine(Bounds.X, Bounds.Y + Bounds.Height, Bounds.X, Bounds.Y + Bounds.Height);

            path.CloseFigure();
            return path;
        }

        static GraphicsPath RoundedRectanglePath(RectangleF Bounds, float Radius, RoundAngles Angles)
        {
            float Diameter = FixRoundRadius(Radius, Bounds.Width, Bounds.Height) * 2f;
            RectangleF arc = new RectangleF(Bounds.Location, new SizeF(Diameter, Diameter));
            GraphicsPath path = new GraphicsPath();

            if (Diameter <= 0f)
            {
                path.AddRectangle(Bounds);
                return path;
            }

            if (HasFlag(Angles, RoundAngles.TopLeft))
                path.AddArc(arc, 180f, 90f);
            else
                path.AddLine(Bounds.Location, Bounds.Location);

            arc.X = Bounds.Right - Diameter;

            if (HasFlag(Angles, RoundAngles.TopRight))
                path.AddArc(arc, 270f, 90f);
            else
                path.AddLine(Bounds.X + Bounds.Width, Bounds.Y, Bounds.X + Bounds.Width, Bounds.Y);

            arc.Y = Bounds.Bottom - Diameter;

            if (HasFlag(Angles, RoundAngles.BottomRight))
                path.AddArc(arc, 0f, 90f);
            else
                path.AddLine(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height,
                    Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);

            arc.X = Bounds.Left;

            if (HasFlag(Angles, RoundAngles.BottomLeft))
                path.AddArc(arc, 90f, 90f);
            else
                path.AddLine(Bounds.X, Bounds.Y + Bounds.Height, Bounds.X, Bounds.Y + Bounds.Height);

            path.CloseFigure();
            return path;
        }
        #endregion

        /// <summary>
        /// Taken from http://stackoverflow.com/questions/9033/hidden-features-of-c/407325#407325
        /// instead of doing (enum & value) == value you can now use enum.Has(value)
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="type">The enum value you want to test</param>
        /// <param name="value">Flag Enum Value you're looking for</param>
        /// <returns>True if the type has value bit set</returns>
        public static bool HasFlag<T>(Enum type, T value)
        {
            return (((int)(object)type & (int)(object)value) == (int)(object)value);
        }

        public static int FixRoundRadius(int Radius, int Width, int Height)
        {
            int HalfWidth = Width / 2;
            int HalfHeight = Height / 2;

            if (HalfWidth < Radius)
                Radius = HalfWidth;
            if (HalfHeight < Radius)
                Radius = HalfHeight;

            return Math.Max(0, Radius);
        }

        public static float FixRoundRadius(float Radius, float Width, float Height)
        {
            float HalfWidth = Width * 0.5f;
            float HalfHeight = Height * 0.5f;

            if (HalfWidth < Radius)
                Radius = HalfWidth;
            if (HalfHeight < Radius)
                Radius = HalfHeight;

            return Math.Max(0, Radius);
        }

        public static Blend FactorPosListToBlend(List<PointF> FactorPos)
        {
            Blend blend = new Blend(FactorPos.Count);
            for (int i = 0; i < FactorPos.Count; i++)
            {
                blend.Factors[i] = FactorPos[i].X;
                blend.Positions[i] = FactorPos[i].Y;
            }

            return blend;
        }
    }
}
