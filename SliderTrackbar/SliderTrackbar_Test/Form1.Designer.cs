﻿namespace SliderTrackbar_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.checkBoxEnabled = new System.Windows.Forms.CheckBox();
            this.sliderTrackbar11 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar10 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar9 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar8 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar7 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar6 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar5 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar4 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar2 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar1 = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar3 = new System.Windows.Forms.SliderTrackbar();
            this.SuspendLayout();
            // 
            // checkBoxEnabled
            // 
            this.checkBoxEnabled.AutoSize = true;
            this.checkBoxEnabled.Checked = true;
            this.checkBoxEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEnabled.Location = new System.Drawing.Point(12, 163);
            this.checkBoxEnabled.Name = "checkBoxEnabled";
            this.checkBoxEnabled.Size = new System.Drawing.Size(65, 17);
            this.checkBoxEnabled.TabIndex = 6;
            this.checkBoxEnabled.Text = "Enabled";
            this.checkBoxEnabled.UseVisualStyleBackColor = true;
            this.checkBoxEnabled.CheckedChanged += new System.EventHandler(this.checkBoxEnabled_CheckedChanged);
            // 
            // sliderTrackbar11
            // 
            this.sliderTrackbar11.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar11.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar11.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar11.BarBlend_FactorPos")));
            this.sliderTrackbar11.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar11.BarBorderBlend_FactorPos")));
            this.sliderTrackbar11.BarRoundRadius = 2F;
            this.sliderTrackbar11.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar11.BgBlend_FactorPos")));
            this.sliderTrackbar11.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar11.BgBorderBlend_FactorPos")));
            this.sliderTrackbar11.BgRoundRadius = 2F;
            this.sliderTrackbar11.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
            this.sliderTrackbar11.Location = new System.Drawing.Point(106, 163);
            this.sliderTrackbar11.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar11.Name = "sliderTrackbar11";
            this.sliderTrackbar11.SelectedItem = null;
            this.sliderTrackbar11.Size = new System.Drawing.Size(80, 39);
            this.sliderTrackbar11.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.sliderTrackbar11.TabIndex = 14;
            this.sliderTrackbar11.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.sliderTrackbar11.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // sliderTrackbar10
            // 
            this.sliderTrackbar10.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar10.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar10.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar10.BarBlend_FactorPos")));
            this.sliderTrackbar10.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar10.BarBorderBlend_FactorPos")));
            this.sliderTrackbar10.BarRoundRadius = 2F;
            this.sliderTrackbar10.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar10.BgBlend_FactorPos")));
            this.sliderTrackbar10.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar10.BgBorderBlend_FactorPos")));
            this.sliderTrackbar10.BgRoundRadius = 2F;
            this.sliderTrackbar10.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
            this.sliderTrackbar10.Location = new System.Drawing.Point(192, 163);
            this.sliderTrackbar10.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar10.Name = "sliderTrackbar10";
            this.sliderTrackbar10.SelectedItem = null;
            this.sliderTrackbar10.Size = new System.Drawing.Size(80, 39);
            this.sliderTrackbar10.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.sliderTrackbar10.TabIndex = 13;
            this.sliderTrackbar10.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
            this.sliderTrackbar10.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // sliderTrackbar9
            // 
            this.sliderTrackbar9.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar9.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar9.ArrowsIconEnabled = false;
            this.sliderTrackbar9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar9.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar9.BarBlend_FactorPos")));
            this.sliderTrackbar9.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar9.BarBorderBlend_FactorPos")));
            this.sliderTrackbar9.BarRoundRadius = 2F;
            this.sliderTrackbar9.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar9.BgBlend_FactorPos")));
            this.sliderTrackbar9.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar9.BgBorderBlend_FactorPos")));
            this.sliderTrackbar9.BgRoundRadius = 2F;
            this.sliderTrackbar9.Location = new System.Drawing.Point(410, 12);
            this.sliderTrackbar9.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar9.Name = "sliderTrackbar9";
            this.sliderTrackbar9.SelectedItem = null;
            this.sliderTrackbar9.Size = new System.Drawing.Size(38, 237);
            this.sliderTrackbar9.TabIndex = 12;
            this.sliderTrackbar9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sliderTrackbar9.TextStringFormat = "{0:0.###}%";
            this.sliderTrackbar9.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.sliderTrackbar9.ValueDefault = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // sliderTrackbar8
            // 
            this.sliderTrackbar8.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar8.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar8.AutoOrientation = true;
            this.sliderTrackbar8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar8.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar8.BarBlend_FactorPos")));
            this.sliderTrackbar8.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar8.BarBorderBlend_FactorPos")));
            this.sliderTrackbar8.BarRoundRadius = 2F;
            this.sliderTrackbar8.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar8.BgBlend_FactorPos")));
            this.sliderTrackbar8.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar8.BgBorderBlend_FactorPos")));
            this.sliderTrackbar8.BgRoundRadius = 2F;
            this.sliderTrackbar8.InverseLayout = true;
            this.sliderTrackbar8.Location = new System.Drawing.Point(366, 12);
            this.sliderTrackbar8.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar8.Name = "sliderTrackbar8";
            this.sliderTrackbar8.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.sliderTrackbar8.SelectedItem = null;
            this.sliderTrackbar8.Size = new System.Drawing.Size(38, 237);
            this.sliderTrackbar8.TabIndex = 11;
            this.sliderTrackbar8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sliderTrackbar8.TextStringFormat = "{0:0.###}%";
            this.sliderTrackbar8.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.sliderTrackbar8.ValueDefault = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // sliderTrackbar7
            // 
            this.sliderTrackbar7.ApplyLayoutToArrows = false;
            this.sliderTrackbar7.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar7.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar7.AutoOrientation = true;
            this.sliderTrackbar7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar7.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar7.BarBlend_FactorPos")));
            this.sliderTrackbar7.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar7.BarBorderBlend_FactorPos")));
            this.sliderTrackbar7.BarRoundRadius = 2F;
            this.sliderTrackbar7.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar7.BgBlend_FactorPos")));
            this.sliderTrackbar7.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar7.BgBorderBlend_FactorPos")));
            this.sliderTrackbar7.BgRoundRadius = 2F;
            this.sliderTrackbar7.InverseLayout = true;
            this.sliderTrackbar7.Location = new System.Drawing.Point(322, 12);
            this.sliderTrackbar7.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar7.Name = "sliderTrackbar7";
            this.sliderTrackbar7.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.sliderTrackbar7.SelectedItem = null;
            this.sliderTrackbar7.Size = new System.Drawing.Size(38, 237);
            this.sliderTrackbar7.TabIndex = 10;
            this.sliderTrackbar7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sliderTrackbar7.TextStringFormat = "{0:0.###}%";
            this.sliderTrackbar7.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.sliderTrackbar7.ValueDefault = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // sliderTrackbar6
            // 
            this.sliderTrackbar6.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar6.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar6.AutoOrientation = true;
            this.sliderTrackbar6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar6.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar6.BarBlend_FactorPos")));
            this.sliderTrackbar6.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar6.BarBorderBlend_FactorPos")));
            this.sliderTrackbar6.BarRoundRadius = 2F;
            this.sliderTrackbar6.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar6.BgBlend_FactorPos")));
            this.sliderTrackbar6.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar6.BgBorderBlend_FactorPos")));
            this.sliderTrackbar6.BgRoundRadius = 2F;
            this.sliderTrackbar6.Location = new System.Drawing.Point(278, 12);
            this.sliderTrackbar6.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar6.Name = "sliderTrackbar6";
            this.sliderTrackbar6.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.sliderTrackbar6.SelectedItem = null;
            this.sliderTrackbar6.Size = new System.Drawing.Size(38, 237);
            this.sliderTrackbar6.TabIndex = 9;
            this.sliderTrackbar6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sliderTrackbar6.TextStringFormat = "{0:0.###}%";
            this.sliderTrackbar6.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.sliderTrackbar6.ValueDefault = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // sliderTrackbar5
            // 
            this.sliderTrackbar5.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar5.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar5.ArrowsIconEnabled = false;
            this.sliderTrackbar5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar5.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar5.BarBlend_FactorPos")));
            this.sliderTrackbar5.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar5.BarBorderBlend_FactorPos")));
            this.sliderTrackbar5.BarBorderWidth = 2F;
            this.sliderTrackbar5.BarRoundRadius = 5F;
            this.sliderTrackbar5.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar5.BgBlend_FactorPos")));
            this.sliderTrackbar5.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar5.BgBorderBlend_FactorPos")));
            this.sliderTrackbar5.BgBorderWidth = 2F;
            this.sliderTrackbar5.BgRoundRadius = 5F;
            this.sliderTrackbar5.InverseLayout = true;
            this.sliderTrackbar5.Location = new System.Drawing.Point(12, 208);
            this.sliderTrackbar5.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.sliderTrackbar5.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar5.Name = "sliderTrackbar5";
            this.sliderTrackbar5.SelectedItem = null;
            this.sliderTrackbar5.Size = new System.Drawing.Size(260, 41);
            this.sliderTrackbar5.SliderMaximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.sliderTrackbar5.TabIndex = 8;
            this.sliderTrackbar5.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.sliderTrackbar5.ValueDefault = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // sliderTrackbar4
            // 
            this.sliderTrackbar4.ArrayOfValues = new string[0];
            this.sliderTrackbar4.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar4.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar4.ArrowsIconScale = 0.4F;
            this.sliderTrackbar4.ArrowsIconTrianglesSize = 0.55F;
            this.sliderTrackbar4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar4.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar4.BarBlend_FactorPos")));
            this.sliderTrackbar4.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar4.BarBorderBlend_FactorPos")));
            this.sliderTrackbar4.BarRoundAngles = System.Drawing.GraphicsExt.RoundAngles.BottomRight;
            this.sliderTrackbar4.BarRoundRadius = 10F;
            this.sliderTrackbar4.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar4.BgBlend_FactorPos")));
            this.sliderTrackbar4.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar4.BgBorderBlend_FactorPos")));
            this.sliderTrackbar4.BgRoundAngles = System.Drawing.GraphicsExt.RoundAngles.BottomRight;
            this.sliderTrackbar4.BgRoundRadius = 10F;
            this.sliderTrackbar4.Location = new System.Drawing.Point(12, 133);
            this.sliderTrackbar4.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            65536});
            this.sliderTrackbar4.Minimum = new decimal(new int[] {
            35,
            0,
            0,
            65536});
            this.sliderTrackbar4.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar4.Name = "sliderTrackbar4";
            this.sliderTrackbar4.SelectedItem = null;
            this.sliderTrackbar4.Size = new System.Drawing.Size(260, 24);
            this.sliderTrackbar4.SliderMaximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.sliderTrackbar4.SliderMinimum = new decimal(new int[] {
            35,
            0,
            0,
            65536});
            this.sliderTrackbar4.Step = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.sliderTrackbar4.TabIndex = 7;
            this.sliderTrackbar4.Value = new decimal(new int[] {
            35,
            0,
            0,
            65536});
            this.sliderTrackbar4.ValueDefault = new decimal(new int[] {
            35,
            0,
            0,
            65536});
            // 
            // sliderTrackbar2
            // 
            this.sliderTrackbar2.ArrayOfValues = new string[] {
        ""};
            this.sliderTrackbar2.ArrowKeysSliderRange = true;
            this.sliderTrackbar2.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar2.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar2.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar2.BarBlend_FactorPos")));
            this.sliderTrackbar2.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar2.BarBorderBlend_FactorPos")));
            this.sliderTrackbar2.BarBorderColor1_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BarBorderColor1_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BarBorderColor1_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BarBorderColor2_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BarBorderColor2_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BarBorderColor2_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BarColor1 = System.Drawing.Color.Gray;
            this.sliderTrackbar2.BarColor1_Disabled = System.Drawing.Color.Gray;
            this.sliderTrackbar2.BarColor1_Focused = System.Drawing.Color.Gray;
            this.sliderTrackbar2.BarColor1_Targeted = System.Drawing.Color.Gray;
            this.sliderTrackbar2.BarColor2 = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar2.BarColor2_Disabled = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar2.BarColor2_Focused = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar2.BarColor2_Targeted = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar2.BarRoundRadius = 2F;
            this.sliderTrackbar2.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar2.BgBlend_FactorPos")));
            this.sliderTrackbar2.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar2.BgBorderBlend_FactorPos")));
            this.sliderTrackbar2.BgBorderColor1_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BgBorderColor1_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BgBorderColor1_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BgBorderColor2_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BgBorderColor2_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BgBorderColor2_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar2.BgColor1 = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor1_Disabled = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor1_Focused = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor1_Targeted = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor2 = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor2_Disabled = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor2_Focused = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgColor2_Targeted = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar2.BgRoundRadius = 2F;
            this.sliderTrackbar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sliderTrackbar2.Location = new System.Drawing.Point(12, 39);
            this.sliderTrackbar2.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.sliderTrackbar2.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar2.Name = "sliderTrackbar2";
            this.sliderTrackbar2.SelectedItem = "";
            this.sliderTrackbar2.Size = new System.Drawing.Size(260, 39);
            this.sliderTrackbar2.TabIndex = 4;
            this.sliderTrackbar2.TextStringFormat = "Value: {0:0.###}";
            this.sliderTrackbar2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.sliderTrackbar2.ValueDefault = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // sliderTrackbar1
            // 
            this.sliderTrackbar1.ArrayOfValues = new string[] {
        ""};
            this.sliderTrackbar1.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar1.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar1.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar1.BarBlend_FactorPos")));
            this.sliderTrackbar1.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar1.BarBorderBlend_FactorPos")));
            this.sliderTrackbar1.BarBorderColor1_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BarBorderColor1_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BarBorderColor1_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BarBorderColor2_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BarBorderColor2_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BarBorderColor2_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BarColor1 = System.Drawing.Color.Gray;
            this.sliderTrackbar1.BarColor1_Disabled = System.Drawing.Color.Gray;
            this.sliderTrackbar1.BarColor1_Focused = System.Drawing.Color.Gray;
            this.sliderTrackbar1.BarColor1_Targeted = System.Drawing.Color.Gray;
            this.sliderTrackbar1.BarColor2 = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar1.BarColor2_Disabled = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar1.BarColor2_Focused = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar1.BarColor2_Targeted = System.Drawing.Color.Gainsboro;
            this.sliderTrackbar1.BarRoundRadius = 2F;
            this.sliderTrackbar1.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar1.BgBlend_FactorPos")));
            this.sliderTrackbar1.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar1.BgBorderBlend_FactorPos")));
            this.sliderTrackbar1.BgBorderColor1_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BgBorderColor1_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BgBorderColor1_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BgBorderColor2_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BgBorderColor2_Focused = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BgBorderColor2_Targeted = System.Drawing.Color.DimGray;
            this.sliderTrackbar1.BgColor1 = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor1_Disabled = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor1_Focused = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor1_Targeted = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor2 = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor2_Disabled = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor2_Focused = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgColor2_Targeted = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar1.BgRoundRadius = 2F;
            this.sliderTrackbar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sliderTrackbar1.Location = new System.Drawing.Point(12, 12);
            this.sliderTrackbar1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.sliderTrackbar1.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.sliderTrackbar1.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar1.Name = "sliderTrackbar1";
            this.sliderTrackbar1.SelectedItem = "";
            this.sliderTrackbar1.Size = new System.Drawing.Size(260, 21);
            this.sliderTrackbar1.SliderMaximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.sliderTrackbar1.SliderMinimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.sliderTrackbar1.Step = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.sliderTrackbar1.TabIndex = 0;
            this.sliderTrackbar1.TextStringFormat = "Value: {0:0.###}";
            this.sliderTrackbar1.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.sliderTrackbar1.ValueDefault = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // sliderTrackbar3
            // 
            this.sliderTrackbar3.ArrayOfValues = new string[] {
        "Alfa",
        "Bravo",
        "Charlie",
        "Delta",
        "Echo",
        "Foxtrot",
        "Golf",
        "Hotel",
        "India",
        "Juliett",
        "Kilo",
        "Lima",
        "Mike",
        "November",
        "Oscar",
        "Papa",
        "Quebec",
        "Romeo",
        "Sierra",
        "Tango",
        "Uniform",
        "Victor",
        "Whiskey",
        "X-ray",
        "Yankee",
        "Zulu"};
            this.sliderTrackbar3.ArrayOfValuesEnabled = true;
            this.sliderTrackbar3.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar3.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar3.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar3.BarBlend_FactorPos")));
            this.sliderTrackbar3.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar3.BarBorderBlend_FactorPos")));
            this.sliderTrackbar3.BarRoundRadius = 2F;
            this.sliderTrackbar3.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar3.BgBlend_FactorPos")));
            this.sliderTrackbar3.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar3.BgBorderBlend_FactorPos")));
            this.sliderTrackbar3.BgRoundRadius = 2F;
            this.sliderTrackbar3.Location = new System.Drawing.Point(12, 84);
            this.sliderTrackbar3.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar3.Name = "sliderTrackbar3";
            this.sliderTrackbar3.SelectedIndex = 14;
            this.sliderTrackbar3.SelectedItem = "Oscar";
            this.sliderTrackbar3.Size = new System.Drawing.Size(260, 50);
            this.sliderTrackbar3.TabIndex = 5;
            this.sliderTrackbar3.TextStringFormat = "Value: {0:0.###}";
            this.sliderTrackbar3.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(462, 267);
            this.Controls.Add(this.sliderTrackbar11);
            this.Controls.Add(this.sliderTrackbar10);
            this.Controls.Add(this.sliderTrackbar9);
            this.Controls.Add(this.sliderTrackbar8);
            this.Controls.Add(this.sliderTrackbar7);
            this.Controls.Add(this.sliderTrackbar6);
            this.Controls.Add(this.sliderTrackbar5);
            this.Controls.Add(this.sliderTrackbar4);
            this.Controls.Add(this.checkBoxEnabled);
            this.Controls.Add(this.sliderTrackbar2);
            this.Controls.Add(this.sliderTrackbar1);
            this.Controls.Add(this.sliderTrackbar3);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SliderTrackbar Test";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SliderTrackbar sliderTrackbar1;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar2;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar3;
        private System.Windows.Forms.CheckBox checkBoxEnabled;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar4;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar5;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar6;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar7;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar8;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar9;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar10;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar11;
    }
}

