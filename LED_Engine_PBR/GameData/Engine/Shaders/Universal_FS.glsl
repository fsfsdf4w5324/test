#version 330
uniform sampler2D TextureUnit0; // BaseColor or Albedo (RGBA)
uniform sampler2D TextureUnit1; // Normal
uniform sampler2D TextureUnit2; // Roughness
uniform sampler2D TextureUnit3; // Metalness
uniform sampler2D TextureUnit4; // Emissive
uniform sampler2D TextureUnit5; // Ambient Occlusion
uniform sampler2D TextureUnit6; // Height

in vec3 f_Position;
in vec2 f_UV;
in mat3 f_TBN; // VectorWS = TBN * VectorTS, WS - World space, TS - Tangent space

uniform bool Use_Parallax_Mapping = false;
#include("ParallaxMapping.glsl");

#include("MaterialInfo.glsl");
uniform MaterialInfo Material;
uniform mat4 ModelMatrix;
uniform vec3 CameraPos;

layout(location = 0) out vec4 Output0;
layout(location = 1) out vec4 Output1;
layout(location = 2) out vec4 Output2;
layout(location = 3) out vec4 Output3;

void main()
{
	vec2 UV = f_UV;
	vec2 UV_DDX = dFdx(UV);
	vec2 UV_DDY = dFdy(UV);
	
	if (Use_Parallax_Mapping && TexUnits[6])
	{
		vec3 CameraVectorWS = normalize(CameraPos - f_Position);
		vec3 CameraVectorTS = normalize(transpose(f_TBN) * CameraVectorWS);
		vec3 SurfaceNormalWS = normalize(f_TBN[2]);
		UV = ParallaxMapping(TextureUnit6, CameraVectorTS, CameraVectorWS, SurfaceNormalWS, UV, UV_DDX, UV_DDY);
	}
	
	vec4  BaseColor = (TexUnits[0] ? Material.BC * texture(TextureUnit0, UV)     : Material.BC);
	vec3  Emissive  = (TexUnits[4] ? Material.E  * texture(TextureUnit4, UV).rgb : vec3(0.0));
	float AmbientOcclusion = (TexUnits[5] ? mix(1.0, texture(TextureUnit5, UV).r, Material.AO) : 1.0);
	float Roughness = (TexUnits[2] ? texture(TextureUnit2, UV).r : Material.R);
	float Metalness = (TexUnits[3] ? texture(TextureUnit3, UV).r : Material.M);
	float Height    = (TexUnits[6] ? texture(TextureUnit6, UV).r : 0.0);
	
	vec3 NormalWS = normalize(TexUnits[1] ? f_TBN * (textureGrad(TextureUnit1, UV, UV_DDX, UV_DDY).rgb * 2.0 - 1.0) : f_TBN[2]);
	
	if (BaseColor.a <= 0.02)
		discard;
	
	Output0 = vec4(BaseColor.rgb, Roughness);
	Output1 = vec4(NormalWS, Metalness);
	Output2 = vec4(f_Position, AmbientOcclusion);
	Output3 = vec4(Emissive, Height); // Height used for debug only!!!
}