#version 330
uniform samplerCube TextureUnit0; // Environment map

uniform float Roughness;

in vec3 f_Position;

layout(location = 0) out vec3 FragColor;

#include("Functions.glsl")

float DistributionGGX(float NdotH, float AlphaRoughnessSquared)
{
	float denom = NdotH * NdotH * (AlphaRoughnessSquared - 1.0) + 1.0;
	return AlphaRoughnessSquared / (PI * denom * denom);
}

void main()
{
	float AlphaRoughness = Roughness * Roughness; // Disney's Roughness [see Burley'12 siggraph]
	float AlphaRoughnessSquared = AlphaRoughness * AlphaRoughness;
	
	// Make the simplyfying assumption that V equals R equals the Normal
	vec3 N = normalize(f_Position); // V = R = N
	mat3 TBN = ImportanceSampleTBN(N);
	
	vec3 SpecularValue = vec3(0.0);
	float TotalWeight = 0.0;

	const uint SAMPLE_COUNT = 32u;
	for(uint i = 0u; i < SAMPLE_COUNT; i++)
	{
		// generates a sample vector that's biased towards the preferred alignment direction (importance sampling).
		//vec2 Xi = Hammersley(i, SAMPLE_COUNT);
		vec2 Xi = Hammersley32[i]; // SAMPLE_COUNT dependent!!!
		vec3 H = TBN * ImportanceSampleGGX(Xi, AlphaRoughnessSquared);
		vec3 L = -normalize(reflect(N, H));
		float NdotL = max(dot(N, L), 0.0);
		
		if(NdotL > 0.0)
		{
			float NdotH = max(dot(N, H), 0.0);
			// Sample from the environment's mip level based on roughness/pdf
			float D = DistributionGGX(NdotH, AlphaRoughnessSquared);
			float PDF = D * 0.25; // D * NdotH / (4 * VdotH), but N = V
			
			float TextureSize = textureSize(TextureUnit0, 0).x;
			// SamplesPerTexel = Solid angle Sample / Solid angle Texel
			float SamplesPerTexel = (1.5 * TextureSize * TextureSize) / (SAMPLE_COUNT * PDF * PI);
			const float MipBias = 1.0;
			float MipLevel = (Roughness <= 0.0 ? 0.0 : max(0.5 * log2(SamplesPerTexel) + MipBias, 0.0));
			SpecularValue += textureLod(TextureUnit0, L, MipLevel).rgb * NdotL;
			TotalWeight += NdotL;
		}
	}
	
	FragColor = (TotalWeight <= 0.0 ? SpecularValue : SpecularValue / TotalWeight);
	// FragColor = vec3(0.0); // DEBUG ONLY
}