#version 330
uniform sampler2D TextureUnit0; // Equirectangular texture

in vec3 f_Position;

layout(location = 0) out vec4 FragColor;

const vec2 invAtan = vec2(0.159154943, 0.318309886); // (1/Two_PI = 0.159, 1/PI = 0.318)

void main()
{
	vec3 Dir = normalize(f_Position);
	vec2 UV = vec2(atan(Dir.z, Dir.x), asin(Dir.y)) * invAtan + 0.5;
	vec3 Color = texture(TextureUnit0, UV).rgb;
	FragColor = vec4(Color, 1.0);
}