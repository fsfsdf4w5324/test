#version 330
uniform samplerCube TextureUnit0; // Environment map

in vec3 f_Position;

layout(location = 0) out vec3 FragColor; // Irradiance map

#include("Functions.glsl")

void main()
{
	vec3 N = normalize(f_Position);
	mat3 TBN = ImportanceSampleTBN(N);
	vec3 DiffuseValue = vec3(0.0);
	float TotalWeight = 0.0;
	
	const uint SAMPLE_COUNT = 16u;
	for(uint i = 0u; i < SAMPLE_COUNT; i++)
	{
		// generates a sample vector that's biased towards the preferred alignment direction (importance sampling).
		//vec2 Xi = Hammersley(i, SAMPLE_COUNT);
		vec2 Xi = Hammersley16[i]; // SAMPLE_COUNT dependent!!!
		vec3 H = TBN * ImportanceSampleDiffuse(Xi);
		vec3 L = -normalize(reflect(N, H)); // Must be V instead N, but V=N
		float NdotL = max(dot(N, L), 0.0);
		
		if (NdotL > 0.0)
		{
			// Compute Lod using inverse solid angle and pdf.
			// From Chapter 20.4 Mipmap filtered samples in GPU Gems 3.
			// http://http.developer.nvidia.com/GPUGems3/gpugems3_ch20.html
			float PDF = max(0.0, dot(N, L)/* * INV_PI*/);
			
			float TextureSize = textureSize(TextureUnit0, 0).x;
			// SamplesPerTexel = Solid angle Sample / Solid angle Texel
			float SamplesPerTexel = (1.5 * TextureSize * TextureSize) / (/*(*/SAMPLE_COUNT * PDF/*) * PI*/);
			const float MipBias = 1.0;
			float MipLevel = 0.5 * log2(SamplesPerTexel) + MipBias;
			
			DiffuseValue += textureLod(TextureUnit0, H, MipLevel).rgb;
			TotalWeight++;
		}
	}
	
	FragColor = (TotalWeight <= 0.0 ? DiffuseValue : DiffuseValue / TotalWeight);
	//FragColor = vec3(0.0); // DEBUG ONLY
}