#version 330
uniform sampler2D TextureUnit1; // BaseColor.rgb, Roughness
uniform sampler2D TextureUnit2; // Normal.xyz, Metalness
uniform sampler2D TextureUnit3; // PositionW.xyz, AmbientOcclusion
uniform samplerCube TextureUnit7; // IBL Specular Environment

in vec2 f_UV;

uniform vec3 CameraPos;

layout(location = 0) out vec4 FragColor;

void main()
{
	vec3 Normal = texture(TextureUnit2, f_UV).rgb;
	vec3 Position = texture(TextureUnit3, f_UV).rgb;
	vec3 ViewDir = normalize(CameraPos - Position);	// Get View vector (from surface to camera)
	vec3 Reflected = -normalize(reflect(ViewDir, Normal)); // Reflected vector
	
	const float MIN_ROUGHNESS = 0.04;
	float Roughness = texture(TextureUnit1, f_UV).a;
	float PerceptualRoughness = clamp(Roughness, MIN_ROUGHNESS, 1.0);
	
	const float MipsCount = 9.0; // resolution of 512x512, Must be same as in program
    float LOD = PerceptualRoughness * MipsCount;
	vec3 IBL_Specular = textureLod(TextureUnit7, Reflected, LOD).rgb;
	
	FragColor = vec4(IBL_Specular, 1.0);
}