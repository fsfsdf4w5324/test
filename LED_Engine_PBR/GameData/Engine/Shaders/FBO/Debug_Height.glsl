#version 330
uniform sampler2D TextureUnit4;

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	float Height = texture(TextureUnit4, f_UV).a;
	Height = sRGB_To_Linear(Height);
	FragColor = vec4(Height, Height, Height, 1.0);
}