// sRGB - images on PC, Albedo, Emissive...
// Linear - NormalMap, data in Buffers, Shaders

// sRGB -> Linear
/*float sRGB_To_Linear(float Value)
{
	float CutOff = step(0.0404482362771082, Value);
    float Higher = Value / 12.92;
    float Lower = pow((Value + 0.055) / 1.055, 2.4);
    return mix(Higher, Lower, CutOff);
}

vec2 sRGB_To_Linear(vec2 Color)
{
	vec2 CutOff = step(vec2(0.0404482362771082), Color);
    vec2 Higher = Color / vec2(12.92);
    vec2 Lower = pow((Color + vec2(0.055)) / vec2(1.055), vec2(2.4));
    return mix(Higher, Lower, CutOff);
}

vec3 sRGB_To_Linear(vec3 Color)
{
	vec3 CutOff = step(vec3(0.0404482362771082), Color);
    vec3 Higher = Color / vec3(12.92);
    vec3 Lower = pow((Color + vec3(0.055)) / vec3(1.055), vec3(2.4));
    return mix(Higher, Lower, CutOff);
}

vec4 sRGB_To_Linear(vec4 Color)
{
	vec3 CutOff = step(vec3(0.0404482362771082), Color.rgb);
    vec3 Higher = Color.rgb / vec3(12.92);
    vec3 Lower = pow((Color.rgb + vec3(0.055)) / vec3(1.055), vec3(2.4));
	return vec4(mix(Higher, Lower, CutOff), Color.a);
}*/

float sRGB_To_Linear/*_Fast*/(float Value)
{
	return pow(Value, 2.2);
}

vec2 sRGB_To_Linear/*_Fast*/(vec2 Color)
{
	return pow(Color, vec2(2.2));
}

vec3 sRGB_To_Linear/*_Fast*/(vec3 Color)
{
	return pow(Color, vec3(2.2));
}

vec4 sRGB_To_Linear/*_Fast*/(vec4 Color)
{
	return vec4(pow(Color.rgb, vec3(2.2)), Color.a);
}

// Linear -> sRGB
/*float Linear_To_sRGB(float Value)
{
	float CutOff = step(0.00313066844250063, Value);
    float Higher = 1.055 * pow(Value, 1.0 / 2.4) - 0.055;
    float Lower = Value * 12.92;
    return mix(Higher, Lower, CutOff);
}

vec2 Linear_To_sRGB(vec2 Color)
{
    vec2 CutOff = step(vec2(0.00313066844250063), Color);
    vec2 Higher = vec2(1.055) * pow(Color, vec2(1.0 / 2.4)) - vec2(0.055);
    vec2 Lower = Color * vec2(12.92);
    return mix(Higher, Lower, CutOff);
}

vec3 Linear_To_sRGB(vec3 Color)
{
    vec3 CutOff = step(vec3(0.00313066844250063), Color);
    vec3 Higher = vec3(1.055) * pow(Color, vec3(1.0 / 2.4)) - vec3(0.055);
    vec3 Lower = Color * vec3(12.92);
    return mix(Higher, Lower, CutOff);
}

vec4 Linear_To_sRGB(vec4 Color)
{
    vec3 CutOff = step(vec3(0.00313066844250063), Color.rgb);
    vec3 Higher = vec3(1.055) * pow(Color.rgb, vec3(1.0 / 2.4)) - vec3(0.055);
    vec3 Lower = Color.rgb * vec3(12.92);
    return vec4(mix(Higher, Lower, CutOff), Color.a);
}*/

float Linear_To_sRGB/*_Fast*/(float Value)
{
	return pow(Value, 1.0 / 2.2);
}

vec2 Linear_To_sRGB/*_Fast*/(vec2 Color)
{
	return pow(Color, vec2(1.0 / 2.2));
}

vec3 Linear_To_sRGB/*_Fast*/(vec3 Color)
{
	return pow(Color, vec3(1.0 / 2.2));
}

vec4 Linear_To_sRGB/*_Fast*/(vec4 Color)
{
	return vec4(pow(Color.rgb, vec3(1.0 / 2.2)), Color.a);
}