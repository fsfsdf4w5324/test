#version 330
uniform sampler2D TextureUnit1; // BaseColor.rgb, Roughness
uniform sampler2D TextureUnit2; // Normal.xyz, Metalness
uniform sampler2D TextureUnit3; // PositionW.xyz, AmbientOcclusion
uniform sampler2D TextureUnit5; // BRDF LUT

in vec2 f_UV;

uniform vec3 CameraPos;

layout(location = 0) out vec4 FragColor;

void main()
{
	vec3 Normal = texture(TextureUnit2, f_UV).rgb;
	vec3 Position = texture(TextureUnit3, f_UV).rgb;
	vec3 ViewDir = normalize(CameraPos - Position);	// Get View vector (from surface to camera)
	
	const float MIN_ROUGHNESS = 0.04;
	float Roughness = texture(TextureUnit1, f_UV).a;
	float PerceptualRoughness = clamp(Roughness, MIN_ROUGHNESS, 1.0);
	float NdotV = clamp(abs(dot(Normal, ViewDir)), 0.001, 1.0);
	
	vec2 BRDF = texture(TextureUnit5, vec2(NdotV, PerceptualRoughness)).rg;
	
	FragColor = vec4(BRDF, 0.0, 1.0);
}