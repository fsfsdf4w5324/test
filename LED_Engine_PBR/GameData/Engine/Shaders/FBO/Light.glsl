#include("LightInfo.glsl")

#define Max_DirectionalLights 4
#define Max_PointLights 100
#define Max_SpotLights 50

uniform DirectionalLightInfo	LightD[Max_DirectionalLights];
uniform PointLightInfo			LightP[Max_PointLights];
uniform SpotLightInfo			LightS[Max_SpotLights];
uniform int LDcount;			//Directional Light Count
uniform int LPcount;			//Directional Light Count
uniform int LScount;			//Directional Light Count

#IncludeOnceString("uniform vec3 CameraPos;")

const float M_PI = 3.14159265358979323846;
const float MIN_ROUGHNESS = 0.04;

#ifndef BRDF_LUT
#define BRDF_LUT TextureUnit5
#endif

#ifndef IBL_Diff
#define IBL_Diff TextureUnit6
#endif

#ifndef IBL_Spec
#define IBL_Spec TextureUnit7
#endif

float Attenuation(float FalloffExponent, vec3 Distance)
{
	return 1.0 / pow(length(Distance), FalloffExponent);
}

// The following equation models the Fresnel reflectance term of the spec equation (aka F())
vec3 SpecularReflection(vec3 Reflectance0, vec3 Reflectance90, float VdotH)
{
	return Reflectance0 + (Reflectance90 - Reflectance0) * pow(clamp(1.0 - VdotH, 0.0, 1.0), 5.0);
}

// This calculates the specular geometric attenuation (aka G()),
// where rougher material will reflect less light back to the viewer.
float GeometricOcclusion(float AlphaRoughnessSquared, float NdotL, float NdotV)
{
	float AttenuationL = 2.0 * NdotL / (NdotL + sqrt(AlphaRoughnessSquared + (1.0 - AlphaRoughnessSquared) * (NdotL * NdotL)));
	float AttenuationV = 2.0 * NdotV / (NdotV + sqrt(AlphaRoughnessSquared + (1.0 - AlphaRoughnessSquared) * (NdotV * NdotV)));
	return AttenuationL * AttenuationV;
}

// The following equation(s) model the distribution of microfacet normals across the area being drawn (aka D())
// Implementation from "Average Irregularity Representation of a Roughened Surface for Ray Reflection" by T. S. Trowbridge, and K. P. Reitz
float MicrofacetDistribution(float AlphaRoughnessSquared, float NdotH)
{
	float f = (NdotH * AlphaRoughnessSquared - NdotH) * NdotH + 1.0;
	return AlphaRoughnessSquared / (M_PI * f * f);
}

// Basic Lambertian diffuse
vec3 LambertianDiffuse(vec3 DiffuseColor)
{
    return DiffuseColor / M_PI;
}

// Calculation of the lighting contribution from an optional Image Based Light source.
// Precomputed Environment Maps are required uniform inputs.
vec3 IBLContribution(float PerceptualRoughness, float NdotV, vec3 Normal, vec3 Reflected, vec3 DiffuseColor, vec3 SpecularColor)
{
	vec3 DiffuseLight = texture(IBL_Diff, Normal).rgb;
	vec3 Diffuse = DiffuseLight * DiffuseColor;
	
	vec2 BRDF = texture(BRDF_LUT, vec2(NdotV, PerceptualRoughness)).rg;
	float TextureSize = textureSize(IBL_Spec, 0).x;
	float MipsCount = log2(TextureSize);
    float LOD = PerceptualRoughness * MipsCount;
    vec3 SpecularLight = textureLod(IBL_Spec, Reflected, LOD).rgb;
	vec3 Specular = SpecularLight * (SpecularColor * BRDF.x + BRDF.y);
	
    return Diffuse + Specular;
}

vec3 PBR_BRDF_DirectionalLight(DirectionalLightInfo LI, vec3 N, vec3 V, float NdotV, vec3 SpecularEnvironmentR0, vec3 SpecularEnvironmentR90, float AlphaRoughnessSquared, vec3 DiffuseColor)
{
	vec3 L = normalize(LI.Dir); // Get Light vector (from surface to light)
	vec3 H = normalize(V + L); // Half-way vector
	float NdotL = clamp(dot(N, L), 0.001, 1.0);
	float NdotH = clamp(dot(N, H), 0.0, 1.0);
	float LdotH = clamp(dot(L, H), 0.0, 1.0);
	float VdotH = clamp(dot(V, H), 0.0, 1.0);
	
	// Calculate the shading terms for the microfacet specular shading model
	vec3 F = SpecularReflection(SpecularEnvironmentR0, SpecularEnvironmentR90, VdotH);
	float G = GeometricOcclusion(AlphaRoughnessSquared, NdotL, NdotV);
	float D = MicrofacetDistribution(AlphaRoughnessSquared, NdotH);
	
	// Calculation of analytical lighting contribution
	vec3 DiffuseContribution = (1.0 - F) * LambertianDiffuse(DiffuseColor);
	vec3 SpecularContribution = F * G * D / (4.0 * NdotL * NdotV);
	
	// Obtain final intensity as reflectance (BRDF) scaled by the energy of the light (cosine law)
	return NdotL * LI.Int * (DiffuseContribution + SpecularContribution);
}

vec3 PBR_BRDF_PointLight(PointLightInfo LI, vec3 N, vec3 V, vec3 Pos, float NdotV, vec3 SpecularEnvironmentR0, vec3 SpecularEnvironmentR90, float AlphaRoughnessSquared, vec3 DiffuseColor)
{
	vec3 P = LI.Pos - Pos;
	vec3 L = normalize(P); // Get Light vector (from surface to light)
	vec3 H = normalize(V + L); // Half-way vector
	float NdotL = clamp(dot(N, L), 0.001, 1.0);
	float NdotH = clamp(dot(N, H), 0.0, 1.0);
	float LdotH = clamp(dot(L, H), 0.0, 1.0);
	float VdotH = clamp(dot(V, H), 0.0, 1.0);
	
	// Calculate the shading terms for the microfacet specular shading model
	vec3 F = SpecularReflection(SpecularEnvironmentR0, SpecularEnvironmentR90, VdotH);
	float G = GeometricOcclusion(AlphaRoughnessSquared, NdotL, NdotV);
	float D = MicrofacetDistribution(AlphaRoughnessSquared, NdotH);
	
	// Calculation of analytical lighting contribution
	vec3 DiffuseContribution = (1.0 - F) * LambertianDiffuse(DiffuseColor);
	vec3 SpecularContribution = F * G * D / (4.0 * NdotL * NdotV);
	
	// Obtain final intensity as reflectance (BRDF) scaled by the energy of the light (cosine law)
	vec3 Intensity = LI.Int * Attenuation(LI.Att, P);
	return NdotL * Intensity * (DiffuseContribution + SpecularContribution);
}

vec3 PBR_BRDF_SpotLight(SpotLightInfo LI, vec3 N, vec3 V, vec3 Pos, float NdotV, vec3 SpecularEnvironmentR0, vec3 SpecularEnvironmentR90, float AlphaRoughnessSquared, vec3 DiffuseColor)
{
	vec3 P = LI.Pos - Pos;
	vec3 L = normalize(P); // Get Light vector (from surface to light)
	vec3 H = normalize(V + L); // Half-way vector
	float NdotL = clamp(dot(N, L), 0.001, 1.0);
	float NdotH = clamp(dot(N, H), 0.0, 1.0);
	float LdotH = clamp(dot(L, H), 0.0, 1.0);
	float VdotH = clamp(dot(V, H), 0.0, 1.0);
	
	// Calculate the shading terms for the microfacet specular shading model
	vec3 F = SpecularReflection(SpecularEnvironmentR0, SpecularEnvironmentR90, VdotH);
	float G = GeometricOcclusion(AlphaRoughnessSquared, NdotL, NdotV);
	float D = MicrofacetDistribution(AlphaRoughnessSquared, NdotH);
	
	// Calculation of analytical lighting contribution
	vec3 DiffuseContribution = (1.0 - F) * LambertianDiffuse(DiffuseColor);
	vec3 SpecularContribution = F * G * D / (4.0 * NdotL * NdotV);
	
	// SpotLight factor
	float LDotDir = dot(-L, LI.Dir);
	float SpotLightFactor = (acos(LDotDir) < LI.Cut ? pow(LDotDir, LI.Exp) : 0.0);
	
	// Obtain final intensity as reflectance (BRDF) scaled by the energy of the light (cosine law)
	vec3 Intensity = LI.Int * Attenuation(LI.Att, P) * SpotLightFactor;
	return NdotL * Intensity * (DiffuseContribution + SpecularContribution);
}

vec3 CalcLight(vec3 Position, vec3 Normal, vec3 ViewDir, vec3 BaseColor, float Roughness, float Metalness, float AmbientOcclusion, vec3 Emissive)
{
	float PerceptualRoughness = clamp(Roughness, MIN_ROUGHNESS, 1.0);
    float AlphaRoughness = PerceptualRoughness * PerceptualRoughness; // Linearize Roughness
	float AlphaRoughnessSquared = AlphaRoughness * AlphaRoughness;
	
	vec3 Reflected = -normalize(reflect(ViewDir, Normal)); // Reflected vector
	
	//Reflection Adjusment proposed by the Frostbite team
	float OneMinusAlphaRoughness = 1.0 - AlphaRoughness;
	Reflected = mix(Normal, Reflected, OneMinusAlphaRoughness * (sqrt(OneMinusAlphaRoughness) + AlphaRoughness));
	
	
	float NdotV = clamp(abs(dot(Normal, ViewDir)), 0.001, 1.0);
	
	float MetalnessValue = clamp(Metalness, 0.0, 1.0);
	
	const float f0 = 0.04;
	vec3 DiffuseColor = BaseColor * (1.0 - f0) * (1.0 - MetalnessValue);
	vec3 SpecularColor = mix(vec3(f0), BaseColor, MetalnessValue);
	
	float Reflectance = max(max(SpecularColor.r, SpecularColor.g), SpecularColor.b);
    float Reflectance90 = clamp(Reflectance * 25.0, 0.0, 1.0);
	
    vec3 SpecularEnvironmentR0 = SpecularColor;
    vec3 SpecularEnvironmentR90 = vec3(Reflectance90);

	vec3 AccumColor = vec3(0.0);
	
	for(int i = 0; i < Max_DirectionalLights; i++) // Directional Lights
	{
		if (i < LDcount)
			AccumColor += PBR_BRDF_DirectionalLight(LightD[i], Normal, ViewDir, NdotV, SpecularEnvironmentR0, SpecularEnvironmentR90, AlphaRoughnessSquared, DiffuseColor);
		else
			break;
	}

	for(int i = 0; i < Max_PointLights; i++) // Point Lights
	{
		if (i < LPcount)
			AccumColor += PBR_BRDF_PointLight(LightP[i], Normal, ViewDir, Position, NdotV, SpecularEnvironmentR0, SpecularEnvironmentR90, AlphaRoughnessSquared, DiffuseColor);
		else
			break;
	}
	
	for(int i = 0; i < Max_SpotLights; i++) // Spot Lights
	{
		if (i < LScount)
			AccumColor += PBR_BRDF_SpotLight(LightS[i], Normal, ViewDir, Position, NdotV, SpecularEnvironmentR0, SpecularEnvironmentR90, AlphaRoughnessSquared, DiffuseColor);
		else
			break;
	}
	
	// Calculate lighting contribution from image based lighting source (IBL)
	AccumColor += IBLContribution(PerceptualRoughness, NdotV, Normal, Reflected, DiffuseColor, SpecularColor);
	
	AccumColor *= AmbientOcclusion;
	AccumColor += Emissive;
	
	// Debug
	//AccumColor = BaseColor;
	//AccumColor = vec3(MetalnessValue);
	//AccumColor = vec3(PerceptualRoughness);
	//AccumColor = vec3(texture(BRDF_LUT, vec2(NdotV, PerceptualRoughness)).rg, 1.0);
	//AccumColor = texture(IBL_Diff, Reflected).rgb;
	//AccumColor = textureLod(IBL_Spec, Reflected, 0.0).rgb;
	
	return	AccumColor;
}