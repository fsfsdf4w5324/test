#version 330
uniform sampler2D TextureUnit3;

in vec2 f_UV;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	float AmbientOcclusion = texture(TextureUnit3, f_UV).a;
	AmbientOcclusion = sRGB_To_Linear(AmbientOcclusion);
	FragColor = vec4(AmbientOcclusion, AmbientOcclusion, AmbientOcclusion, 1.0);
}