#version 330
uniform sampler2D TextureUnit2;

in vec2 f_UV;

uniform mat4 ViewMatrix;

layout(location = 0) out vec4 FragColor;

#include("SRGB_To_Linear.glsl")

void main()
{
	vec3 NormalWS = texture(TextureUnit2, f_UV).rgb;
	vec3 NormalVS = vec3(ViewMatrix * vec4(NormalWS, 0.0));
	vec3 Normal = NormalVS * 0.5 + 0.5;
	Normal = sRGB_To_Linear(Normal);
	FragColor = vec4(Normal, 1.0);
}