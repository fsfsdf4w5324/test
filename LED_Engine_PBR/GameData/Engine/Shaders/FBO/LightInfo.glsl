struct DirectionalLightInfo {
	vec3 Dir;		// Direction
	vec3 Int;		// Intensity
};

struct PointLightInfo {
	vec3 Pos;		// Position
	vec3 Int;		// Intensity
	float Att;		// Attenuation exponent
};

struct SpotLightInfo {
	vec3 Pos;		// Position
	vec3 Dir;		// Direction for SpotLight
	vec3 Int;		// Intensity
	float Att;		// Attenuation exponent
	float Cut;		// CutOFF Angle in degrees [0..90]
	float Exp;		// Angular attenuation Exponent
};