struct MaterialInfo {
	vec4  BC;	// BaseColor or Albedo (RGBA)
	float R;	// Roughness
	float M;	// Metalness
	float E;	// Emissive intensity
	float AO;	// Ambient Occlusion
};

// TexUnits[] is used for check, have Material some texture in TextureUnit or not.
//uniform bool TexUnits[min(min(gl_MaxTextureImageUnits, gl_MaxVertexTextureImageUnits), gl_MaxGeometryTextureImageUnits)];
uniform bool TexUnits[gl_MaxTextureImageUnits]; // Use Texture Units? (Check Fragment shader only for optimization)