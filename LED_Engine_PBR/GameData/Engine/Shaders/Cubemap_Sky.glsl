#version 330
uniform samplerCube TextureUnit0;

in vec3 f_Position;

layout (location = 0) out vec4 FragColor;

void main()
{
	FragColor = textureLod(TextureUnit0, f_Position, 0.0);
}