﻿namespace LED_Engine_PBR
{
    partial class LightSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LightSettings));
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Del = new System.Windows.Forms.Button();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.comboBox_Type = new System.Windows.Forms.ComboBox();
            this.listBox_Lights = new System.Windows.Forms.ListBox();
            this.checkBox_LEnabled = new System.Windows.Forms.CheckBox();
            this.button_Serialize = new System.Windows.Forms.Button();
            this.button_Deserialize = new System.Windows.Forms.Button();
            this.button_MoveLDown = new System.Windows.Forms.Button();
            this.button_MoveLUp = new System.Windows.Forms.Button();
            this.sliderTrackbar_Pitch = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_Yaw = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_PosZ = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_PosY = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_PosX = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_Attenuation = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_Intensity = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_SpotLight_Exp = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_SpotLight_CutOff = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_B = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_G = new System.Windows.Forms.SliderTrackbar();
            this.sliderTrackbar_R = new System.Windows.Forms.SliderTrackbar();
            this.SuspendLayout();
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(12, 165);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(20, 20);
            this.button_Add.TabIndex = 1;
            this.button_Add.Text = "+";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // button_Del
            // 
            this.button_Del.Location = new System.Drawing.Point(38, 166);
            this.button_Del.Name = "button_Del";
            this.button_Del.Size = new System.Drawing.Size(20, 20);
            this.button_Del.TabIndex = 2;
            this.button_Del.Text = "-";
            this.button_Del.UseVisualStyleBackColor = true;
            this.button_Del.Click += new System.EventHandler(this.button_Del_Click);
            // 
            // textBox_Name
            // 
            this.textBox_Name.Location = new System.Drawing.Point(238, 154);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(211, 20);
            this.textBox_Name.TabIndex = 4;
            this.textBox_Name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_Name.TextChanged += new System.EventHandler(this.SomeChanged);
            // 
            // comboBox_Type
            // 
            this.comboBox_Type.FormattingEnabled = true;
            this.comboBox_Type.Location = new System.Drawing.Point(64, 166);
            this.comboBox_Type.Name = "comboBox_Type";
            this.comboBox_Type.Size = new System.Drawing.Size(116, 21);
            this.comboBox_Type.TabIndex = 11;
            this.comboBox_Type.SelectedIndexChanged += new System.EventHandler(this.SomeChanged);
            // 
            // listBox_Lights
            // 
            this.listBox_Lights.FormattingEnabled = true;
            this.listBox_Lights.Location = new System.Drawing.Point(12, 12);
            this.listBox_Lights.Name = "listBox_Lights";
            this.listBox_Lights.Size = new System.Drawing.Size(220, 147);
            this.listBox_Lights.TabIndex = 25;
            this.listBox_Lights.Click += new System.EventHandler(this.checkedListBox_Lights_Click);
            this.listBox_Lights.SelectedIndexChanged += new System.EventHandler(this.checkedListBox_Lights_Click);
            this.listBox_Lights.DoubleClick += new System.EventHandler(this.checkedListBox_Lights_Click);
            // 
            // checkBox_LEnabled
            // 
            this.checkBox_LEnabled.AutoSize = true;
            this.checkBox_LEnabled.Location = new System.Drawing.Point(459, 156);
            this.checkBox_LEnabled.Name = "checkBox_LEnabled";
            this.checkBox_LEnabled.Size = new System.Drawing.Size(65, 17);
            this.checkBox_LEnabled.TabIndex = 26;
            this.checkBox_LEnabled.Text = "Enabled";
            this.checkBox_LEnabled.UseVisualStyleBackColor = true;
            this.checkBox_LEnabled.CheckedChanged += new System.EventHandler(this.SomeChanged);
            // 
            // button_Serialize
            // 
            this.button_Serialize.Location = new System.Drawing.Point(530, 152);
            this.button_Serialize.Name = "button_Serialize";
            this.button_Serialize.Size = new System.Drawing.Size(65, 22);
            this.button_Serialize.TabIndex = 27;
            this.button_Serialize.Text = "Serialize";
            this.button_Serialize.UseVisualStyleBackColor = true;
            this.button_Serialize.Click += new System.EventHandler(this.button_Serialize_Click);
            // 
            // button_Deserialize
            // 
            this.button_Deserialize.Location = new System.Drawing.Point(601, 152);
            this.button_Deserialize.Name = "button_Deserialize";
            this.button_Deserialize.Size = new System.Drawing.Size(69, 22);
            this.button_Deserialize.TabIndex = 28;
            this.button_Deserialize.Text = "Deserialize";
            this.button_Deserialize.UseVisualStyleBackColor = true;
            this.button_Deserialize.Click += new System.EventHandler(this.button_Deserialize_Click);
            // 
            // button_MoveLDown
            // 
            this.button_MoveLDown.Location = new System.Drawing.Point(212, 165);
            this.button_MoveLDown.Name = "button_MoveLDown";
            this.button_MoveLDown.Size = new System.Drawing.Size(20, 20);
            this.button_MoveLDown.TabIndex = 29;
            this.button_MoveLDown.Text = "↓";
            this.button_MoveLDown.UseVisualStyleBackColor = true;
            this.button_MoveLDown.Click += new System.EventHandler(this.button_MoveLDown_Click);
            // 
            // button_MoveLUp
            // 
            this.button_MoveLUp.Location = new System.Drawing.Point(186, 165);
            this.button_MoveLUp.Name = "button_MoveLUp";
            this.button_MoveLUp.Size = new System.Drawing.Size(20, 20);
            this.button_MoveLUp.TabIndex = 30;
            this.button_MoveLUp.Text = "↑";
            this.button_MoveLUp.UseVisualStyleBackColor = true;
            this.button_MoveLUp.Click += new System.EventHandler(this.button_MoveLUp_Click);
            // 
            // sliderTrackbar_Pitch
            // 
            this.sliderTrackbar_Pitch.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Pitch.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_Pitch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_Pitch.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Pitch.BarBlend_FactorPos")));
            this.sliderTrackbar_Pitch.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Pitch.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_Pitch.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Pitch.BgBlend_FactorPos")));
            this.sliderTrackbar_Pitch.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Pitch.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_Pitch.Location = new System.Drawing.Point(455, 41);
            this.sliderTrackbar_Pitch.Maximum = new decimal(new int[] {
            720,
            0,
            0,
            0});
            this.sliderTrackbar_Pitch.Minimum = new decimal(new int[] {
            720,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_Pitch.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_Pitch.Name = "sliderTrackbar_Pitch";
            this.sliderTrackbar_Pitch.SelectedItem = null;
            this.sliderTrackbar_Pitch.Size = new System.Drawing.Size(215, 20);
            this.sliderTrackbar_Pitch.SliderMaximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.sliderTrackbar_Pitch.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_Pitch.TabIndex = 42;
            this.sliderTrackbar_Pitch.TextStringFormat = "Pitch Angle: {0:0.###}°";
            this.sliderTrackbar_Pitch.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.sliderTrackbar_Pitch.ValueDefault = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.sliderTrackbar_Pitch.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_Yaw
            // 
            this.sliderTrackbar_Yaw.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Yaw.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_Yaw.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_Yaw.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Yaw.BarBlend_FactorPos")));
            this.sliderTrackbar_Yaw.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Yaw.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_Yaw.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Yaw.BgBlend_FactorPos")));
            this.sliderTrackbar_Yaw.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Yaw.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_Yaw.Location = new System.Drawing.Point(238, 41);
            this.sliderTrackbar_Yaw.Maximum = new decimal(new int[] {
            720,
            0,
            0,
            0});
            this.sliderTrackbar_Yaw.Minimum = new decimal(new int[] {
            720,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_Yaw.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_Yaw.Name = "sliderTrackbar_Yaw";
            this.sliderTrackbar_Yaw.SelectedItem = null;
            this.sliderTrackbar_Yaw.Size = new System.Drawing.Size(211, 20);
            this.sliderTrackbar_Yaw.SliderMaximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.sliderTrackbar_Yaw.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_Yaw.TabIndex = 41;
            this.sliderTrackbar_Yaw.TextStringFormat = "Yaw Angle: {0:0.###}°";
            this.sliderTrackbar_Yaw.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.sliderTrackbar_Yaw.ValueDefault = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.sliderTrackbar_Yaw.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_PosZ
            // 
            this.sliderTrackbar_PosZ.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosZ.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_PosZ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_PosZ.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosZ.BarBlend_FactorPos")));
            this.sliderTrackbar_PosZ.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosZ.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_PosZ.BarColor1 = System.Drawing.Color.LightSkyBlue;
            this.sliderTrackbar_PosZ.BarColor1_Disabled = System.Drawing.Color.LightSteelBlue;
            this.sliderTrackbar_PosZ.BarColor1_Focused = System.Drawing.Color.LightSkyBlue;
            this.sliderTrackbar_PosZ.BarColor1_Targeted = System.Drawing.Color.LightSkyBlue;
            this.sliderTrackbar_PosZ.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosZ.BgBlend_FactorPos")));
            this.sliderTrackbar_PosZ.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosZ.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_PosZ.Location = new System.Drawing.Point(530, 15);
            this.sliderTrackbar_PosZ.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sliderTrackbar_PosZ.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_PosZ.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_PosZ.Name = "sliderTrackbar_PosZ";
            this.sliderTrackbar_PosZ.SelectedItem = null;
            this.sliderTrackbar_PosZ.Size = new System.Drawing.Size(140, 20);
            this.sliderTrackbar_PosZ.SliderMaximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.sliderTrackbar_PosZ.SliderMinimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_PosZ.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_PosZ.TabIndex = 40;
            this.sliderTrackbar_PosZ.TextStringFormat = "Position Z: {0:0.###}";
            this.sliderTrackbar_PosZ.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_PosY
            // 
            this.sliderTrackbar_PosY.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosY.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_PosY.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_PosY.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosY.BarBlend_FactorPos")));
            this.sliderTrackbar_PosY.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosY.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_PosY.BarColor1 = System.Drawing.Color.LightGreen;
            this.sliderTrackbar_PosY.BarColor1_Disabled = System.Drawing.Color.DarkSeaGreen;
            this.sliderTrackbar_PosY.BarColor1_Focused = System.Drawing.Color.LightGreen;
            this.sliderTrackbar_PosY.BarColor1_Targeted = System.Drawing.Color.LightGreen;
            this.sliderTrackbar_PosY.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosY.BgBlend_FactorPos")));
            this.sliderTrackbar_PosY.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosY.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_PosY.Location = new System.Drawing.Point(384, 15);
            this.sliderTrackbar_PosY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sliderTrackbar_PosY.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_PosY.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_PosY.Name = "sliderTrackbar_PosY";
            this.sliderTrackbar_PosY.SelectedItem = null;
            this.sliderTrackbar_PosY.Size = new System.Drawing.Size(140, 20);
            this.sliderTrackbar_PosY.SliderMaximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.sliderTrackbar_PosY.SliderMinimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_PosY.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_PosY.TabIndex = 39;
            this.sliderTrackbar_PosY.TextStringFormat = "Position Y: {0:0.###}";
            this.sliderTrackbar_PosY.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_PosX
            // 
            this.sliderTrackbar_PosX.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosX.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_PosX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_PosX.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosX.BarBlend_FactorPos")));
            this.sliderTrackbar_PosX.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosX.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_PosX.BarColor1 = System.Drawing.Color.LightCoral;
            this.sliderTrackbar_PosX.BarColor1_Disabled = System.Drawing.Color.IndianRed;
            this.sliderTrackbar_PosX.BarColor1_Focused = System.Drawing.Color.LightCoral;
            this.sliderTrackbar_PosX.BarColor1_Targeted = System.Drawing.Color.LightCoral;
            this.sliderTrackbar_PosX.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosX.BgBlend_FactorPos")));
            this.sliderTrackbar_PosX.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_PosX.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_PosX.Location = new System.Drawing.Point(238, 15);
            this.sliderTrackbar_PosX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sliderTrackbar_PosX.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_PosX.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_PosX.Name = "sliderTrackbar_PosX";
            this.sliderTrackbar_PosX.SelectedItem = null;
            this.sliderTrackbar_PosX.Size = new System.Drawing.Size(140, 20);
            this.sliderTrackbar_PosX.SliderMaximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.sliderTrackbar_PosX.SliderMinimum = new decimal(new int[] {
            30,
            0,
            0,
            -2147483648});
            this.sliderTrackbar_PosX.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_PosX.TabIndex = 38;
            this.sliderTrackbar_PosX.TextStringFormat = "Position X: {0:0.###}";
            this.sliderTrackbar_PosX.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_Attenuation
            // 
            this.sliderTrackbar_Attenuation.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Attenuation.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_Attenuation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_Attenuation.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Attenuation.BarBlend_FactorPos")));
            this.sliderTrackbar_Attenuation.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Attenuation.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_Attenuation.BarColor1 = System.Drawing.Color.Gray;
            this.sliderTrackbar_Attenuation.BarColor1_Disabled = System.Drawing.Color.DimGray;
            this.sliderTrackbar_Attenuation.BarColor1_Focused = System.Drawing.Color.Gray;
            this.sliderTrackbar_Attenuation.BarColor1_Targeted = System.Drawing.Color.Gray;
            this.sliderTrackbar_Attenuation.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Attenuation.BgBlend_FactorPos")));
            this.sliderTrackbar_Attenuation.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Attenuation.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_Attenuation.Location = new System.Drawing.Point(455, 93);
            this.sliderTrackbar_Attenuation.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_Attenuation.Name = "sliderTrackbar_Attenuation";
            this.sliderTrackbar_Attenuation.SelectedItem = null;
            this.sliderTrackbar_Attenuation.Size = new System.Drawing.Size(215, 20);
            this.sliderTrackbar_Attenuation.SliderMaximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.sliderTrackbar_Attenuation.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_Attenuation.TabIndex = 37;
            this.sliderTrackbar_Attenuation.TextStringFormat = "Attenuation Exp: {0:0.###}";
            this.sliderTrackbar_Attenuation.Value = new decimal(new int[] {
            22,
            0,
            0,
            65536});
            this.sliderTrackbar_Attenuation.ValueDefault = new decimal(new int[] {
            22,
            0,
            0,
            65536});
            this.sliderTrackbar_Attenuation.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_Intensity
            // 
            this.sliderTrackbar_Intensity.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Intensity.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_Intensity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_Intensity.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Intensity.BarBlend_FactorPos")));
            this.sliderTrackbar_Intensity.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Intensity.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_Intensity.BarColor1 = System.Drawing.Color.Gold;
            this.sliderTrackbar_Intensity.BarColor1_Disabled = System.Drawing.Color.PaleGoldenrod;
            this.sliderTrackbar_Intensity.BarColor1_Focused = System.Drawing.Color.Gold;
            this.sliderTrackbar_Intensity.BarColor1_Targeted = System.Drawing.Color.Gold;
            this.sliderTrackbar_Intensity.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Intensity.BgBlend_FactorPos")));
            this.sliderTrackbar_Intensity.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_Intensity.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_Intensity.Location = new System.Drawing.Point(238, 93);
            this.sliderTrackbar_Intensity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.sliderTrackbar_Intensity.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_Intensity.Name = "sliderTrackbar_Intensity";
            this.sliderTrackbar_Intensity.SelectedItem = null;
            this.sliderTrackbar_Intensity.Size = new System.Drawing.Size(211, 20);
            this.sliderTrackbar_Intensity.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.sliderTrackbar_Intensity.TabIndex = 36;
            this.sliderTrackbar_Intensity.TextStringFormat = "Light Intensity: {0:0.###}";
            this.sliderTrackbar_Intensity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_Intensity.ValueDefault = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_Intensity.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_SpotLight_Exp
            // 
            this.sliderTrackbar_SpotLight_Exp.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_Exp.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_Exp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_SpotLight_Exp.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_Exp.BarBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_Exp.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_Exp.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_Exp.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_Exp.BgBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_Exp.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_Exp.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_Exp.Location = new System.Drawing.Point(455, 119);
            this.sliderTrackbar_SpotLight_Exp.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sliderTrackbar_SpotLight_Exp.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_SpotLight_Exp.Name = "sliderTrackbar_SpotLight_Exp";
            this.sliderTrackbar_SpotLight_Exp.SelectedItem = null;
            this.sliderTrackbar_SpotLight_Exp.Size = new System.Drawing.Size(215, 20);
            this.sliderTrackbar_SpotLight_Exp.SliderMaximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.sliderTrackbar_SpotLight_Exp.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sliderTrackbar_SpotLight_Exp.TabIndex = 35;
            this.sliderTrackbar_SpotLight_Exp.TextStringFormat = "SpotLight Exp: {0:0.###}";
            this.sliderTrackbar_SpotLight_Exp.Value = new decimal(new int[] {
            20,
            0,
            0,
            65536});
            this.sliderTrackbar_SpotLight_Exp.ValueDefault = new decimal(new int[] {
            20,
            0,
            0,
            65536});
            this.sliderTrackbar_SpotLight_Exp.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_SpotLight_CutOff
            // 
            this.sliderTrackbar_SpotLight_CutOff.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_CutOff.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_CutOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_SpotLight_CutOff.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_CutOff.BarBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_CutOff.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_CutOff.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_CutOff.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_CutOff.BgBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_CutOff.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_SpotLight_CutOff.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_SpotLight_CutOff.Location = new System.Drawing.Point(238, 119);
            this.sliderTrackbar_SpotLight_CutOff.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.sliderTrackbar_SpotLight_CutOff.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_SpotLight_CutOff.Name = "sliderTrackbar_SpotLight_CutOff";
            this.sliderTrackbar_SpotLight_CutOff.SelectedItem = null;
            this.sliderTrackbar_SpotLight_CutOff.Size = new System.Drawing.Size(211, 20);
            this.sliderTrackbar_SpotLight_CutOff.SliderMaximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.sliderTrackbar_SpotLight_CutOff.Step = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.sliderTrackbar_SpotLight_CutOff.TabIndex = 34;
            this.sliderTrackbar_SpotLight_CutOff.TextStringFormat = "SpotLight CutOff Angle: {0:0.###}°";
            this.sliderTrackbar_SpotLight_CutOff.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.sliderTrackbar_SpotLight_CutOff.ValueDefault = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.sliderTrackbar_SpotLight_CutOff.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_B
            // 
            this.sliderTrackbar_B.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_B.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_B.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_B.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_B.BarBlend_FactorPos")));
            this.sliderTrackbar_B.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_B.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_B.BarColor1 = System.Drawing.Color.DodgerBlue;
            this.sliderTrackbar_B.BarColor1_Disabled = System.Drawing.Color.Blue;
            this.sliderTrackbar_B.BarColor1_Focused = System.Drawing.Color.DodgerBlue;
            this.sliderTrackbar_B.BarColor1_Targeted = System.Drawing.Color.DodgerBlue;
            this.sliderTrackbar_B.BarColor2 = System.Drawing.Color.LightBlue;
            this.sliderTrackbar_B.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_B.BgBlend_FactorPos")));
            this.sliderTrackbar_B.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_B.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_B.Location = new System.Drawing.Point(530, 67);
            this.sliderTrackbar_B.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_B.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_B.Name = "sliderTrackbar_B";
            this.sliderTrackbar_B.SelectedItem = null;
            this.sliderTrackbar_B.Size = new System.Drawing.Size(140, 20);
            this.sliderTrackbar_B.SliderMaximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_B.Step = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.sliderTrackbar_B.TabIndex = 33;
            this.sliderTrackbar_B.TextStringFormat = "Color Blue: {0:0.###}";
            this.sliderTrackbar_B.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_B.ValueDefault = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_B.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_G
            // 
            this.sliderTrackbar_G.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_G.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_G.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_G.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_G.BarBlend_FactorPos")));
            this.sliderTrackbar_G.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_G.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_G.BarColor1 = System.Drawing.Color.LimeGreen;
            this.sliderTrackbar_G.BarColor1_Disabled = System.Drawing.Color.Green;
            this.sliderTrackbar_G.BarColor1_Focused = System.Drawing.Color.LimeGreen;
            this.sliderTrackbar_G.BarColor1_Targeted = System.Drawing.Color.LimeGreen;
            this.sliderTrackbar_G.BarColor2 = System.Drawing.Color.LightGreen;
            this.sliderTrackbar_G.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_G.BgBlend_FactorPos")));
            this.sliderTrackbar_G.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_G.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_G.Location = new System.Drawing.Point(384, 67);
            this.sliderTrackbar_G.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_G.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_G.Name = "sliderTrackbar_G";
            this.sliderTrackbar_G.SelectedItem = null;
            this.sliderTrackbar_G.Size = new System.Drawing.Size(140, 20);
            this.sliderTrackbar_G.SliderMaximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_G.Step = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.sliderTrackbar_G.TabIndex = 32;
            this.sliderTrackbar_G.TextStringFormat = "Color Green: {0:0.###}";
            this.sliderTrackbar_G.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_G.ValueDefault = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_G.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // sliderTrackbar_R
            // 
            this.sliderTrackbar_R.ArrowsIconBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_R.ArrowsIconBlend_FactorPos")));
            this.sliderTrackbar_R.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.sliderTrackbar_R.BarBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_R.BarBlend_FactorPos")));
            this.sliderTrackbar_R.BarBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_R.BarBorderBlend_FactorPos")));
            this.sliderTrackbar_R.BarColor1 = System.Drawing.Color.Red;
            this.sliderTrackbar_R.BarColor1_Disabled = System.Drawing.Color.DarkRed;
            this.sliderTrackbar_R.BarColor1_Focused = System.Drawing.Color.Red;
            this.sliderTrackbar_R.BarColor1_Targeted = System.Drawing.Color.Red;
            this.sliderTrackbar_R.BarColor2 = System.Drawing.Color.LightPink;
            this.sliderTrackbar_R.BgBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_R.BgBlend_FactorPos")));
            this.sliderTrackbar_R.BgBorderBlend_FactorPos = ((System.Collections.Generic.List<System.Drawing.PointF>)(resources.GetObject("sliderTrackbar_R.BgBorderBlend_FactorPos")));
            this.sliderTrackbar_R.Location = new System.Drawing.Point(238, 67);
            this.sliderTrackbar_R.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_R.MinimumSize = new System.Drawing.Size(30, 15);
            this.sliderTrackbar_R.Name = "sliderTrackbar_R";
            this.sliderTrackbar_R.SelectedItem = null;
            this.sliderTrackbar_R.Size = new System.Drawing.Size(140, 20);
            this.sliderTrackbar_R.SliderMaximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_R.Step = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.sliderTrackbar_R.TabIndex = 31;
            this.sliderTrackbar_R.TextStringFormat = "Color Red: {0:0.###}";
            this.sliderTrackbar_R.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_R.ValueDefault = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sliderTrackbar_R.ValueChanged += new System.EventHandler(this.SomeChanged);
            // 
            // LightSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 193);
            this.Controls.Add(this.sliderTrackbar_Pitch);
            this.Controls.Add(this.sliderTrackbar_Yaw);
            this.Controls.Add(this.sliderTrackbar_PosZ);
            this.Controls.Add(this.sliderTrackbar_PosY);
            this.Controls.Add(this.sliderTrackbar_PosX);
            this.Controls.Add(this.sliderTrackbar_Attenuation);
            this.Controls.Add(this.sliderTrackbar_Intensity);
            this.Controls.Add(this.sliderTrackbar_SpotLight_Exp);
            this.Controls.Add(this.sliderTrackbar_SpotLight_CutOff);
            this.Controls.Add(this.sliderTrackbar_B);
            this.Controls.Add(this.sliderTrackbar_G);
            this.Controls.Add(this.sliderTrackbar_R);
            this.Controls.Add(this.button_MoveLUp);
            this.Controls.Add(this.button_MoveLDown);
            this.Controls.Add(this.button_Deserialize);
            this.Controls.Add(this.button_Serialize);
            this.Controls.Add(this.checkBox_LEnabled);
            this.Controls.Add(this.listBox_Lights);
            this.Controls.Add(this.comboBox_Type);
            this.Controls.Add(this.textBox_Name);
            this.Controls.Add(this.button_Del);
            this.Controls.Add(this.button_Add);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "LightSettings";
            this.Opacity = 0.95D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Light Control";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LightSettings_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Button button_Del;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.ComboBox comboBox_Type;
        private System.Windows.Forms.ListBox listBox_Lights;
        private System.Windows.Forms.CheckBox checkBox_LEnabled;
        private System.Windows.Forms.Button button_Serialize;
        private System.Windows.Forms.Button button_Deserialize;
        private System.Windows.Forms.Button button_MoveLDown;
        private System.Windows.Forms.Button button_MoveLUp;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_R;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_G;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_B;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_SpotLight_CutOff;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_SpotLight_Exp;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_Intensity;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_Attenuation;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_PosX;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_PosY;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_PosZ;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_Yaw;
        private System.Windows.Forms.SliderTrackbar sliderTrackbar_Pitch;
    }
}