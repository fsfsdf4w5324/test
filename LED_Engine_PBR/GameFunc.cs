﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace LED_Engine_PBR
{
    partial class Game
    {
        public static Camera MainCamera = new Camera();
        public static Mesh SkyBox;

        public static Mesh DebugAmbientLight = new Mesh();
        public static Mesh DebugDirectionalLight = new Mesh();
        public static Mesh DebugPointLight = new Mesh();
        public static Mesh DebugSpotLight = new Mesh();

        static void WindowApplySettings(GameWindow Window)
        {
            #region Window Mode
            Window.WindowBorder = (Settings.Window.Borders ? WindowBorder.Resizable : WindowBorder.Hidden);
            Window.Title = Settings.Window.Title;

            // Move Window To Center
            if (Settings.Window.CenterWindow)
            {
                Settings.Window.X = (Screen.PrimaryScreen.WorkingArea.Width - Settings.Window.Width) / 2;
                Settings.Window.Y = (Screen.PrimaryScreen.WorkingArea.Height - Settings.Window.Height) / 2;
            }

            Window.X = Settings.Window.X;
            Window.Y = Settings.Window.Y;
            Window.Width = Settings.Window.Width;
            Window.Height = Settings.Window.Height;
            #endregion

            Window.MakeCurrent();

            #region Apply render settings AFTER window init
            Log.WriteLine("OpenGL Version: " + GL.GetString(StringName.Version));
            Log.WriteLine("GPU: " + GL.GetString(StringName.Renderer));

            Window.Context.SwapInterval = Settings.Graphics.VSyncSwapInterval; // V-sync
            Window.TargetRenderFrequency = Settings.Window.RefreshRateLimit;

            GL.ClearColor(Color4.Gray);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            #endregion
        }

        static void Draw(Mesh m)
        {
            m.CalculateMatrices(MainCamera);
            FrustumCulling.ExtractFrustum(MainCamera.GetViewMatrix() * MainCamera.GetProjectionMatrix());
            float Scale = Math.Max(m.Scale.X, Math.Max(m.Scale.Y, m.Scale.Z));

            if (FrustumCulling.SphereInFrustum(m.Position, m.BoundingSphere.Outer * Scale))
                for (int i = 0; i < m.Parts.Count; i++)
                    if (FrustumCulling.SphereInFrustum(m.Position + m.Parts[i].BoundingSphere.Position, m.Parts[i].BoundingSphere.Outer * Scale))
                        Draw(m, m.Parts[i]);
        }

        static void Draw(Mesh m, MeshPart v)
        {
            if (v.Material.CullFace)
                GL.Enable(EnableCap.CullFace);
            else
                GL.Disable(EnableCap.CullFace);

            int TempLocation;
            Shader shader = v.Material.Shader;
            GL.UseProgram(shader.ProgramID);

            // Активируем нужный TextureUnit и назначаем текстуру
            for (int i = 0; i < v.Material.Textures.Length; i++)
            {
                if (v.Material.Textures[i] != null)
                {
                    GL.ActiveTexture((TextureUnit)((int)TextureUnit.Texture0 + i));
                    GL.BindTexture(v.Material.Textures[i].TextureTarget, v.Material.Textures[i].ID);
                }
            }
            
            #region Работаем с шейдерами
            #region Камера и матрицы
            // Передаем шейдеру матрицу ModelMatrix, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("ModelMatrix");
            if (TempLocation != -1)
                GL.UniformMatrix4(TempLocation, false, ref m.ModelMatrix);

            // Передаем шейдеру матрицу ViewMatrix, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("ViewMatrix");
            if (TempLocation != -1)
            {
                Matrix4 V = MainCamera.GetViewMatrix();
                GL.UniformMatrix4(TempLocation, false, ref V);
            }

            // Передаем шейдеру матрицу ProjectionMatrix, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("ProjectionMatrix");
            if (TempLocation != -1)
            {
                Matrix4 P = MainCamera.GetProjectionMatrix();
                GL.UniformMatrix4(TempLocation, false, ref P);
            }
            
            // Передаем шейдеру матрицу ModelView, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("ModelView");
            if (TempLocation != -1)
                GL.UniformMatrix4(TempLocation, false, ref m.ModelViewMatrix);

            // Передаем шейдеру матрицу NormalMatrix, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("NormalMatrix");
            if (TempLocation != -1)
            {
                Matrix3 NormalMatrix = new Matrix3(m.ModelMatrix);
                NormalMatrix.Invert();
                NormalMatrix.Transpose();
                GL.UniformMatrix3(TempLocation, false, ref NormalMatrix);
            }

            // Передаем шейдеру матрицу ModelViewProjection, если шейдер поддерживает это (должна быть 100% поддержка).
            TempLocation = shader.GetUniform("MVP");
            if (TempLocation != -1)
                GL.UniformMatrix4(TempLocation, false, ref m.ModelViewProjectionMatrix);

            // Передаем шейдеру позицию камеры, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("CameraPos");
            if (TempLocation != -1)
                GL.Uniform3(TempLocation, MainCamera.Position);
            #endregion

            #region Передача различных параметров шейдерам

            // Передаем шейдеру массив тех TextureUnit-ов, которые исаользуются, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("TexUnits[0]");
            if (TempLocation != -1)
            {
                int[] Arr = new int[v.Material.Textures.Length];
                for (int TUnit = 0; TUnit < v.Material.Textures.Length; TUnit++)
                    Arr[TUnit] = (v.Material.Textures[TUnit] != null ? 1 : 0);
                GL.Uniform1(TempLocation, Arr.Length, Arr);
            }

            #region Material parameters
            // Передаем шейдеру вектор Base Color (Albedo), если шейдер поддерживает это.
            TempLocation = shader.GetUniform("Material.BC");
            if (TempLocation != -1)
                GL.Uniform4(TempLocation, v.Material.BaseColor);

            // Передаем шейдеру значение Roughness, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("Material.R");
            if (TempLocation != -1)
                GL.Uniform1(TempLocation, v.Material.Roughness);

            // Передаем шейдеру значение Metalness, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("Material.M");
            if (TempLocation != -1)
                GL.Uniform1(TempLocation, v.Material.Metalness);

            // Передаем шейдеру значение Emissive scale, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("Material.E");
            if (TempLocation != -1)
                GL.Uniform1(TempLocation, v.Material.Emissive);

            // Передаем шейдеру значение Ambient Occlusion factor, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("Material.AO");
            if (TempLocation != -1)
                GL.Uniform1(TempLocation, v.Material.AmbientOcclusion);
            #endregion

            #region Parallax Mapping
            // Передаем шейдеру Parallax Scale, если шейдер поддерживает это.
            TempLocation = shader.GetUniform("Use_Parallax_Mapping");
            if (TempLocation != -1)
                GL.Uniform1(TempLocation, v.Material.UseParallaxMapping ? 1 : 0);

            if (v.Material.UseParallaxMapping)
            {
                // Передаем шейдеру Parallax Scale, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("Parallax_Scale");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, v.Material.Parallax_Scale);

                // Передаем шейдеру Parallax Minimum Steps, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("Parallax_MinSteps");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, v.Material.Parallax_MinSteps);

                // Передаем шейдеру Parallax Maximum Steps, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("Parallax_MaxSteps");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, v.Material.Parallax_MaxSteps);

                // Передаем шейдеру Parallax DiscardByUVs, если шейдер поддерживает это.
                TempLocation = shader.GetUniform("Parallax_DiscardFragments");
                if (TempLocation != -1)
                    GL.Uniform1(TempLocation, v.Material.Parallax_DiscardFragments ? 1 : 0);
            }
            #endregion

            #endregion
            #endregion

            shader.EnableVertexAttribArrays();

            #region Передаем шейдеру VertexPosition, VertexNormal, VertexUV, VertexTangents
            // Передаем шейдеру буфер позицый вертексов, если шейдер поддерживает это (должна быть 100% поддержка).
            TempLocation = shader.GetAttribute("v_Position");
            if (TempLocation != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, v.VertexBufferID);
                GL.VertexAttribPointer(TempLocation, 3, VertexAttribPointerType.Float, false, 0, 0);
            }

            // Передаем шейдеру буфер нормалей, если шейдер поддерживает это.
            TempLocation = shader.GetAttribute("v_Normal");
            if (TempLocation != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, v.NormalBufferID);
                GL.VertexAttribPointer(TempLocation, 3, VertexAttribPointerType.Float, false, 0, 0);
            }

            // Передаем шейдеру буфер текстурных координат, если шейдер поддерживает это.
            TempLocation = shader.GetAttribute("v_UV");
            if (TempLocation != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, v.UVBufferID);
                GL.VertexAttribPointer(TempLocation, 2, VertexAttribPointerType.Float, false, 0, 0);
            }

            // Передаем шейдеру буфер тангенсов, если шейдер поддерживает это.
            TempLocation = shader.GetAttribute("v_Tangent");
            if (TempLocation != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, v.TangentBufferID);
                GL.VertexAttribPointer(TempLocation, 4, VertexAttribPointerType.Float, false, 0, 0);
            }
            #endregion

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, v.IndexBufferID);
            GL.DrawElements(BeginMode.Triangles, v.Indexes.Length, DrawElementsType.UnsignedInt, 0);

            // If you're using VAOs, then you should not disable attribute arrays, as they are encapsulated in the VAO.
            //shader.DisableVertexAttribArrays();
        }
    }
}
