﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace LED_Engine_PBR
{
    public static class Settings
    {
        public static float UnitsScale = 0.01f; // Система измерений (это значение по умолчанию, 1 unit = 100 см)

        public static DrawMode DrawMode = DrawMode.Default;

        public static class Paths
        {
            public static string
                StartupPath,
                EngineContentPath,
                GameDataPath,

                EngineMaps,
                EngineMeshes,
                EngineTextures,
                EngineCubemapTextures,
                EngineShaders,
                EngineFonts,

                Maps,
                Meshes,
                Textures,
                CubemapTextures,
                Shaders,
                Fonts;

            public static class ContentFiles
            {
                public static string
                    EngineMeshes,
                    EngineTextures,
                    EngineCubemapTextures,
                    EngineMaterials,
                    EngineShaders,

                    Meshes,
                    Textures,
                    CubemapTextures,
                    Materials,
                    Shaders;
            }
        }

        public static class Window
        {
            public static int
                X = 0,
                Y = 0,
                Width = 800,
                Height = 600,

                RefreshRateLimit = 0;

            public static string Title = "";
            public static bool
                CenterWindow = false,
                Borders = true;
        }

        public static class Graphics
        {
            public static int VSyncSwapInterval = 0;

            static int anisotropicFiltering = -1;
            public static int AnisotropicFiltering
            {
                get { return anisotropicFiltering; }
                set
                {
                    switch (value)
                    {
                        case 1: //OFF
                        case 2: //2x
                        case 4: //4x
                        case 8: //8x
                        case 16: //16x
                            Settings.Graphics.anisotropicFiltering = value;
                            break;
                        default:
                            Settings.Graphics.anisotropicFiltering = -1; // Default(Maximum) value (a lot of times = 16x)
                            break;
                    }
                }
            }

            public static class FXAA
            {
                public static bool Enabled = true;

                /// <summary>
                /// fxaaQualitySubpix
                /// Default: 0.75
                /// </summary>
                public static float Subpix = 1.0f;

                /// <summary>
                /// fxaaQualityEdgeThreshold
                /// Default: 0.166
                /// </summary>
                public static float EdgeThreshold = 0.125f;

                /// <summary>
                /// fxaaQualityEdgeThresholdMin
                /// Default: 0.0625
                /// </summary>
                public static float EdgeThresholdMin = 0.0625f;
            }
        }

        public static class GL
        {
            /// <summary>
            /// MaxTextureImageUnits - This is the number of fragment shader texture image units.
            /// </summary>
            public static int MaxTextureImageUnits = 0; //FS
            public static int MaxVertexTextureImageUnits = 0; //VS
            public static int MaxGeometryTextureImageUnits = 0; //GS

            /// <summary>
            /// TextureImageUnits = Min(MaxTextureImageUnits, MaxVertexTextureImageUnits, MaxGeometryTextureImageUnits)
            /// </summary>
            public static int TextureImageUnits = 0;
        }
    }
}
