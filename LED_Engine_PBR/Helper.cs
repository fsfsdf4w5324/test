﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace LED_Engine_PBR
{
    public static class Helper
    {
        /// <summary>
        /// Get Direction vector from Yaw and Pitch (in radians).
        /// </summary>
        /// <param name="Yaw">Yaw in radians.</param>
        /// <param name="Pitch">Pitch in radians.</param>
        /// <returns>Direction vector</returns>
        public static Vector3 FromYawPitch(float Yaw, float Pitch)
        {
            float CosPitch = (float)Math.Cos(Pitch); //Optimization
            Vector3 Result = new Vector3(CosPitch * (float)Math.Sin(Yaw), (float)Math.Sin(Pitch), CosPitch * (float)Math.Cos(Yaw));
            //Result.Normalize();
            return Result;
        }

        /// <summary>
        /// Get Direction vector from Yaw and Pitch (in radians).
        /// </summary>
        /// <param name="Yaw">Yaw in radians.</param>
        /// <param name="Pitch">Pitch in radians.</param>
        /// <returns>Direction vector</returns>
        public static Vector3d FromYawPitch(double Yaw, double Pitch)
        {
            float CosPitch = (float)Math.Cos(Pitch); //Optimization
            Vector3d Result = new Vector3d(CosPitch * Math.Sin(Yaw), Math.Sin(Pitch), CosPitch * Math.Cos(Yaw));
            //Result.Normalize();
            return Result;

        }

        /// <summary>
        /// Get Yaw from Direction vector.
        /// </summary>
        /// <param name="DirectionVector">Direction vector.</param>
        /// <returns>Yaw, Pitch in radians/</returns>
        public static Vector2 ExtractYawPitch(Vector3 V)
        {
            var V_Normalized = V.Normalized();
            float Pitch = (float)Math.Asin(V_Normalized.Y);
            float Yaw = (float)Math.Atan2(V_Normalized.X, V_Normalized.Z);
            return new Vector2(Yaw, Pitch);
        }

        /// <summary>
        /// Get Yaw from Direction vector.
        /// </summary>
        /// <param name="DirectionVector">Direction vector.</param>
        /// <returns>Yaw, Pitch in radians/</returns>
        public static Vector2d ExtractYawPitch(Vector3d V)
        {
            var V_Normalized = V.Normalized();
            double Pitch = Math.Asin(V_Normalized.Y);
            double Yaw = Math.Atan2(V_Normalized.X, V_Normalized.Z);
            return new Vector2d(Yaw, Pitch);
        }

        public static bool IsExtensionSupported(string Extension)
        {
            string[] Extensions = GL.GetString(StringName.Extensions).Split(new char[]{ ' ', '\n', '\r'}, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < Extensions.Length; i++)
                if (Extensions[i] == Extension.Trim())
                    return true;
            return false;
        }
    }
}
