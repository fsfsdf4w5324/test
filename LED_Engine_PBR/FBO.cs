﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;

namespace LED_Engine_PBR
{
    static public class FBO
    {
        public static int // It's not a Shader.ProgramID !!!
            ShaderIndex_P1_Orig,
            ShaderIndex_P1,
            ShaderIndex_PP,
            ShaderIndex_Equirectangular_To_Cubemap_Gen,
            ShaderIndex_BRDF_LUT_Gen,
            ShaderIndex_IBL_Diffuse_Gen,
            ShaderIndex_IBL_Specular_Gen;

        public static int // Debug Shaders Indexes
            DebugShaderIndex_Depth,
            DebugShaderIndex_BaseColor,
            DebugShaderIndex_Normals,
            DebugShaderIndex_Roughness,
            DebugShaderIndex_Metalness,
            DebugShaderIndex_Emissive,
            DebugShaderIndex_AmbientOcclusion,
            DebugShaderIndex_Height,
            DebugShaderIndex_Light,
            DebugShaderIndex_BRDF_LUT,
            DebugShaderIndex_IBL_Diffuse,
            DebugShaderIndex_IBL_Specular;

        public static List<Shader> Shaders = new List<Shader>();

        public static int
            FBO_P1, //Pass1 (G-Buffer)
            FBO_PP; //PostProcess
        public static bool UsePostEffects = true;

        static DrawBuffersEnum[] ColorAttachments_P1 = new DrawBuffersEnum[] {
            DrawBuffersEnum.ColorAttachment0, // BaseColor.rgb, Roughness
            DrawBuffersEnum.ColorAttachment1, // Normals.xyz, Metalness
            DrawBuffersEnum.ColorAttachment2, // Position, AmbientOcclusion
            DrawBuffersEnum.ColorAttachment3};// Emissive, Height (Height used for debug only!!! Channel can used for other purposes.)

        //Pass0 OUT textures
        public static int Texture_Depth;
        public static int[] Textures_P1 = new int[ColorAttachments_P1.Length];

        //Pass1 OUT Textures
        public static int Texture_PP;

        #region PBR Textures (for IBL)
        static int Texture_BRDF_LUT = 0;
        static int Texture_IBL_Diff = 0;
        static int Texture_IBL_Spec = 0;

        static Matrix4 Cubemap_ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(90f), 1f, 0.1f, 10f);
        static Matrix4[] Cubemap_ViewMatrices = new Matrix4[] {
            Matrix4.LookAt(Vector3.Zero, new Vector3( 1f,  0f,  0f), new Vector3(0f, -1f,  0f)),
            Matrix4.LookAt(Vector3.Zero, new Vector3(-1f,  0f,  0f), new Vector3(0f, -1f,  0f)),
            Matrix4.LookAt(Vector3.Zero, new Vector3( 0f,  1f,  0f), new Vector3(0f,  0f,  1f)),
            Matrix4.LookAt(Vector3.Zero, new Vector3( 0f, -1f,  0f), new Vector3(0f,  0f, -1f)),
            Matrix4.LookAt(Vector3.Zero, new Vector3( 0f,  0f,  1f), new Vector3(0f, -1f,  0f)),
            Matrix4.LookAt(Vector3.Zero, new Vector3( 0f,  0f, -1f), new Vector3(0f, -1f,  0f)),
        };
        #endregion

        static int ScreenWidth, ScreenHeight;
        static int VBO;
        static float[] FBO_Vertexes = new float[] {
            -1.0f, -1.0f,
             1.0f, -1.0f,
            -1.0f,  1.0f,
             1.0f,  1.0f }; // Screen vertexes positions

        static void CheckFBO(string FBO_Name)
        {
            FramebufferErrorCode FramebufferStatus = GL.CheckFramebufferStatus(FramebufferTarget.FramebufferExt);
            if (FramebufferStatus != FramebufferErrorCode.FramebufferCompleteExt)
                Log.WriteLineRed("GL.CheckFramebufferStatus: \"" + FBO_Name + "\" error {0}", FramebufferStatus.ToString());
        }

        public static void Init(int ScrWidth, int ScrHeight)
        {
            Free();

            #region Shaders
            for (int i = 0; i < Shaders.Count; i++)
            {
                Shaders[i].LoadShader();

                switch (Shaders[i].Name)
                {
                    case "Deferred":
                        ShaderIndex_P1 = i;
                        ShaderIndex_P1_Orig = i;
                        break;
                    case "PostProcess":
                        ShaderIndex_PP = i;
                        break;

                    // PBR Textures generation
                    case "Equirectangular_To_Cubemap":
                        ShaderIndex_Equirectangular_To_Cubemap_Gen = i;
                        break;
                    case "BRDF_LUT_Gen":
                        ShaderIndex_BRDF_LUT_Gen = i;
                        break;
                    case "IBL_Diffuse":
                        ShaderIndex_IBL_Diffuse_Gen = i;
                        break;
                    case "IBL_Specular":
                        ShaderIndex_IBL_Specular_Gen = i;
                        break;

                    // Debug
                    case "Debug_Depth":
                        DebugShaderIndex_Depth = i;
                        break;
                    case "Debug_BaseColor":
                        DebugShaderIndex_BaseColor = i;
                        break;
                    case "Debug_Normals":
                        DebugShaderIndex_Normals = i;
                        break;
                    case "Debug_Roughness":
                        DebugShaderIndex_Roughness = i;
                        break;
                    case "Debug_Metalness":
                        DebugShaderIndex_Metalness = i;
                        break;
                    case "Debug_Emissive":
                        DebugShaderIndex_Emissive = i;
                        break;
                    case "Debug_AmbientOcclusion":
                        DebugShaderIndex_AmbientOcclusion = i;
                        break;
                    case "Debug_Height":
                        DebugShaderIndex_Height = i;
                        break;
                    case "Debug_Light":
                        DebugShaderIndex_Light = i;
                        break;
                    case "Debug_BRDF_LUT":
                        DebugShaderIndex_BRDF_LUT = i;
                        break;
                    case "Debug_IBL_Diffuse":
                        DebugShaderIndex_IBL_Diffuse = i;
                        break;
                    case "Debug_IBL_Specular":
                        DebugShaderIndex_IBL_Specular = i;
                        break;
                }
            }
            #endregion

            #region Gen Textures for Deffered rendering, Post Processesing
            Texture_Depth = GL.GenTexture();
            GL.GenTextures(Textures_P1.Length, Textures_P1);
            Texture_PP = GL.GenTexture();
            #endregion

            Rescale(ScrWidth, ScrHeight);

            #region Generate FBO's
            //Pass1 (G-Buffer)
            FBO_P1 = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBO_P1);
            GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, TextureTarget.Texture2D, Texture_Depth, 0);
            for (int i = 0; i < Textures_P1.Length; i++)
                GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext + i, TextureTarget.Texture2D, Textures_P1[i], 0);
            GL.DrawBuffers(ColorAttachments_P1.Length, ColorAttachments_P1);
            CheckFBO("FBO_P1 (G-Buffer)");

            //PostProcess FrameBuffer
            FBO_PP = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBO_PP);
            GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, TextureTarget.Texture2D, Texture_Depth, 0);
            GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, Texture_PP, 0);
            CheckFBO("FBO_PostProcess");

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0); //Bind default FrameBuffer
            #endregion

            VBO = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(FBO_Vertexes.Length * sizeof(float)), FBO_Vertexes, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public static void Rescale(int ScrWidth, int ScrHeight)
        {
            ScreenWidth = ScrWidth;
            ScreenHeight = ScrHeight;

            #region Depth
            // Depth
            GL.BindTexture(TextureTarget.Texture2D, Texture_Depth);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent32f, ScreenWidth, ScreenHeight, 0,
                PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            #endregion

            #region Pass1
            for (int i = 0; i < Textures_P1.Length; i++)
            {
                GL.BindTexture(TextureTarget.Texture2D, Textures_P1[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, ScreenWidth, ScreenHeight, 0,
                    PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            }
            #endregion

            #region Post Process
            GL.BindTexture(TextureTarget.Texture2D, Texture_PP);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, ScreenWidth, ScreenHeight, 0,
                PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            #endregion

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public static void Generate_PBR_Textures()
        {
            Generate_2D_BRDF_LUT_Texture();
            int EnvTextureID = (Game.SkyBox != null ? Game.SkyBox.Parts[0].Material.Textures[0] : Textures.GetTexture(0)).ID;
            Texture_IBL_Diff = Generate_IBL_Diffuse(EnvTextureID); // Irradiance map (created from environment map)
            Texture_IBL_Spec = Generate_IBL_Specular(EnvTextureID); // Frefiltered environment map
        }

        public static int Generate_Cubemap_From_Equirectangular_Texture(int Equirectangular_Texture, int Texture_Size = 512)
        {
            int Texture_EnvironmentCubemap = GL.GenTexture();
            GL.BindTexture(TextureTarget.TextureCubeMap, Texture_EnvironmentCubemap);
            for (int i = 0; i < Cubemap_ViewMatrices.Length; i++)
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, PixelInternalFormat.Rgb16f, Texture_Size, Texture_Size, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);

            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);

            GL.UseProgram(Shaders[ShaderIndex_Equirectangular_To_Cubemap_Gen].ProgramID);
            Shaders[ShaderIndex_Equirectangular_To_Cubemap_Gen].EnableVertexAttribArrays();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Equirectangular_Texture);

            GL.Viewport(0, 0, Texture_Size, Texture_Size);
            GL.Disable(EnableCap.DepthTest);

            int EnvCubeMap_FBO = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, EnvCubeMap_FBO);
            MeshPart Cube = MeshPart.MakeBox(1f, true);

            for (int i = 0; i < Cubemap_ViewMatrices.Length; ++i)
            {
                int Uniform_ViewMartix = Shaders[ShaderIndex_Equirectangular_To_Cubemap_Gen].GetUniform("MVP"); // Only View and Projection will be used
                if (Uniform_ViewMartix != -1)
                {
                    Matrix4 ViewProjectionMatrix = Cubemap_ViewMatrices[i] * Cubemap_ProjectionMatrix;
                    GL.UniformMatrix4(Uniform_ViewMartix, false, ref ViewProjectionMatrix);
                }

                GL.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.TextureCubeMapPositiveX + i, Texture_EnvironmentCubemap, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                GL.BindBuffer(BufferTarget.ArrayBuffer, Cube.VertexBufferID);
                GL.VertexAttribPointer(Shaders[ShaderIndex_Equirectangular_To_Cubemap_Gen].GetAttribute("v_Position"), 3, VertexAttribPointerType.Float, false, 0, 0);
                GL.DrawArrays(PrimitiveType.TriangleStrip, 0, Cube.Indexes.Length);
            }

            Shaders[ShaderIndex_Equirectangular_To_Cubemap_Gen].DisableVertexAttribArrays();
            GL.DeleteFramebuffer(EnvCubeMap_FBO);
            Cube.Free();

            GL.BindTexture(TextureTarget.TextureCubeMap, Texture_EnvironmentCubemap);
            GL.GenerateMipmap(GenerateMipmapTarget.TextureCubeMap);

            // Restore default FBO (Screen) and Viewport size
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, ScreenWidth, ScreenHeight);

            return Texture_EnvironmentCubemap;
        }

        public static void Generate_2D_BRDF_LUT_Texture()
        {
            const int BRDF_LUT_Texture_Size = 512;

            Texture_BRDF_LUT = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, Texture_BRDF_LUT);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rg16f, BRDF_LUT_Texture_Size, BRDF_LUT_Texture_Size, 0, PixelFormat.Rg, PixelType.Float, IntPtr.Zero);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            GL.Viewport(0, 0, BRDF_LUT_Texture_Size, BRDF_LUT_Texture_Size);

            int BRDF_LUT_FBO = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, BRDF_LUT_FBO);
            GL.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, Texture_BRDF_LUT, 0);

            GL.Disable(EnableCap.DepthTest);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.UseProgram(Shaders[ShaderIndex_BRDF_LUT_Gen].ProgramID);
            Shaders[ShaderIndex_BRDF_LUT_Gen].EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.VertexAttribPointer(Shaders[ShaderIndex_BRDF_LUT_Gen].GetAttribute("v_UV"), 2, VertexAttribPointerType.Float, false, 0, 0);
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);

            Shaders[ShaderIndex_BRDF_LUT_Gen].DisableVertexAttribArrays();

            GL.DeleteFramebuffer(BRDF_LUT_FBO);

            // Restore default FBO (Screen) and Viewport size
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, ScreenWidth, ScreenHeight);
        }

        public static int Generate_IBL_Diffuse(int Texture_EnvironmentCubemap) // Irradiance
        {
            const int Texture_Size = 64;

            int IBL_Diffuse_Irradiance_Map = GL.GenTexture();
            GL.BindTexture(TextureTarget.TextureCubeMap, IBL_Diffuse_Irradiance_Map);

            for (int i = 0; i < Cubemap_ViewMatrices.Length; i++)
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, PixelInternalFormat.Rgb16f, Texture_Size, Texture_Size, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);

            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);

            GL.UseProgram(Shaders[ShaderIndex_IBL_Diffuse_Gen].ProgramID);
            Shaders[ShaderIndex_IBL_Diffuse_Gen].EnableVertexAttribArrays();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.TextureCubeMap, Texture_EnvironmentCubemap);
            GL.Enable(EnableCap.TextureCubeMapSeamless);

            GL.Viewport(0, 0, Texture_Size, Texture_Size);
            GL.Disable(EnableCap.DepthTest);

            int IBL_Diffuse_FBO = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, IBL_Diffuse_FBO);
            MeshPart Cube = MeshPart.MakeBox(1f, true);

            for (int i = 0; i < Cubemap_ViewMatrices.Length; i++)
            {
                int Uniform_ViewMartix = Shaders[ShaderIndex_IBL_Diffuse_Gen].GetUniform("MVP"); // Only View and Projection will be used
                if (Uniform_ViewMartix != -1)
                {
                    Matrix4 ViewProjectionMatrix = Cubemap_ViewMatrices[i] * Cubemap_ProjectionMatrix;
                    GL.UniformMatrix4(Uniform_ViewMartix, false, ref ViewProjectionMatrix);
                }

                GL.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.TextureCubeMapPositiveX + i, IBL_Diffuse_Irradiance_Map, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                GL.BindBuffer(BufferTarget.ArrayBuffer, Cube.VertexBufferID);
                GL.VertexAttribPointer(Shaders[ShaderIndex_IBL_Diffuse_Gen].GetAttribute("v_Position"), 3, VertexAttribPointerType.Float, false, 0, 0);
                GL.DrawArrays(PrimitiveType.TriangleStrip, 0, Cube.Indexes.Length);
            }

            Shaders[ShaderIndex_IBL_Diffuse_Gen].DisableVertexAttribArrays();
            GL.DeleteFramebuffer(IBL_Diffuse_FBO);
            Cube.Free();

            // Restore default FBO (Screen) and Viewport size
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, ScreenWidth, ScreenHeight);

            return IBL_Diffuse_Irradiance_Map;
        }

        public static int Generate_IBL_Specular(int Texture_EnvironmentCubemap) // Frefiltered environment map
        {
            const int Texture_Size = 512; // Must be same as in shader

            int IBL_Specular_Map = GL.GenTexture();
            GL.BindTexture(TextureTarget.TextureCubeMap, IBL_Specular_Map);

            for (int i = 0; i < Cubemap_ViewMatrices.Length; i++)
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, PixelInternalFormat.Rgb16f, Texture_Size, Texture_Size, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);

            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
            GL.GenerateMipmap(GenerateMipmapTarget.TextureCubeMap);

            GL.UseProgram(Shaders[ShaderIndex_IBL_Specular_Gen].ProgramID);
            Shaders[ShaderIndex_IBL_Specular_Gen].EnableVertexAttribArrays();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.TextureCubeMap, Texture_EnvironmentCubemap);
            GL.Enable(EnableCap.TextureCubeMapSeamless);

            GL.Viewport(0, 0, Texture_Size, Texture_Size);
            GL.Disable(EnableCap.DepthTest);

            int IBL_Specular_FBO = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, IBL_Specular_FBO);
            MeshPart Cube = MeshPart.MakeBox(1f, true);

            int MaxMipLevels = (int)Math.Log(Texture_Size, 2);
            for (int Mip = 0; Mip <= MaxMipLevels; Mip++)
            {
                int Mip_Width = (int)(Texture_Size * Math.Pow(0.5, Mip));
                int Mip_Height = (int)(Texture_Size * Math.Pow(0.5, Mip));
                GL.Viewport(0, 0, Mip_Width, Mip_Height);

                float Roughness = (float)Mip / MaxMipLevels;

                int Uniform_Roughness = Shaders[ShaderIndex_IBL_Specular_Gen].GetUniform("Roughness");
                if (Uniform_Roughness != -1)
                    GL.Uniform1(Uniform_Roughness, Roughness);

                for (int i = 0; i < Cubemap_ViewMatrices.Length; i++)
                {
                    int Uniform_ViewMartix = Shaders[ShaderIndex_IBL_Specular_Gen].GetUniform("MVP"); // Only View and Projection will be used
                    if (Uniform_ViewMartix != -1)
                    {
                        Matrix4 ViewProjectionMatrix = Cubemap_ViewMatrices[i] * Cubemap_ProjectionMatrix;
                        GL.UniformMatrix4(Uniform_ViewMartix, false, ref ViewProjectionMatrix);
                    }

                    GL.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.TextureCubeMapPositiveX + i, IBL_Specular_Map, Mip);
                    GL.Clear(ClearBufferMask.ColorBufferBit);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, Cube.VertexBufferID);
                    GL.VertexAttribPointer(Shaders[ShaderIndex_IBL_Specular_Gen].GetAttribute("v_Position"), 3, VertexAttribPointerType.Float, false, 0, 0);
                    GL.DrawArrays(PrimitiveType.TriangleStrip, 0, Cube.Indexes.Length);
                }
            }
            //

            Shaders[ShaderIndex_IBL_Specular_Gen].DisableVertexAttribArrays();
            GL.DeleteFramebuffer(IBL_Specular_FBO);
            Cube.Free();

            // Restore default FBO (Screen) and Viewport size
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, ScreenWidth, ScreenHeight);

            return IBL_Specular_Map;
        }

        public static void Draw_P1()
        {
            // Bind PostProcess FBO
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, FBO_PP);
            GL.Disable(EnableCap.DepthTest);
            GL.ClearColor(Color4.Gray);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Enable(EnableCap.TextureCubeMapSeamless);

            GL.UseProgram(Shaders[ShaderIndex_P1].ProgramID);
            int TempLocation;

            /*
            TempLocation = Shaders[ShaderIndex_P1].GetUniform("ScreenSize");
            if (TempLocation != -1)
                GL.Uniform2(TempLocation, (float)ScreenWidth, (float)ScreenHeight);
            */

            TempLocation = Shaders[ShaderIndex_P1].GetUniform("ClipPlanes");
            if (TempLocation != -1) //zNear, zFar
                GL.Uniform2(TempLocation, Game.MainCamera.zNear, Game.MainCamera.zFar);

            TempLocation = Shaders[ShaderIndex_P1].GetUniform("ViewMatrix");
            if (TempLocation != -1)
            {
                Matrix4 ViewInv = Game.MainCamera.GetViewMatrix();
                GL.UniformMatrix4(TempLocation, false, ref ViewInv);
            }

            TempLocation = Shaders[ShaderIndex_P1].GetUniform("ProjectionMatrix");
            if (TempLocation != -1)
            {
                Matrix4 ProjecionMatrix = Game.MainCamera.GetProjectionMatrix();
                GL.UniformMatrix4(TempLocation, false, ref ProjecionMatrix);
            }

            TempLocation = Shaders[ShaderIndex_P1].GetUniform("CameraPos");
            if (TempLocation != -1)
                GL.Uniform3(TempLocation, Game.MainCamera.Position);

            //TempLocation = Shaders[ShaderIndex_P1].GetUniform("Time");
            //if (TempLocation != -1)
            //{
            //    float Time = (float)Glfw.GetTime();
            //    GL.Uniform1(TempLocation, Time);
            //}

            #region Lights
            TempLocation = Shaders[ShaderIndex_P1].GetUniform("LDcount");
            if (TempLocation != -1)
            {
                string IndexStr;
                List<Light> L_Directional = new List<Light>();
                List<Light> L_Point = new List<Light>();
                List<Light> L_Spot = new List<Light>();

                foreach (Light L in Lights.LIGHTS)
                {
                    if (L.Enabled)
                    {
                        switch (L.Type)
                        {
                            case LightType.Directional:
                                L_Directional.Add(L);
                                break;
                            case LightType.Point:
                                L_Point.Add(L);
                                break;
                            case LightType.Spot:
                                L_Spot.Add(L);
                                break;
                        }
                    }
                }

                GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LDcount"), L_Directional.Count);
                GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LPcount"), L_Point.Count);
                GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LScount"), L_Spot.Count);

                // DirectionalLight
                for (int i = 0; i < L_Directional.Count; i++)
                {
                    IndexStr = i.ToString();
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightD[" + IndexStr + "].Dir"), L_Directional[i].Direction);
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightD[" + IndexStr + "].Int"), L_Directional[i].Color * L_Directional[i].Intensity);
                }

                // PointLight
                for (int i = 0; i < L_Point.Count; i++)
                {
                    IndexStr = i.ToString();
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightP[" + IndexStr + "].Pos"), L_Point[i].Position);
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightP[" + IndexStr + "].Int"), L_Point[i].Color * L_Point[i].Intensity);
                    GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LightP[" + IndexStr + "].Att"), L_Point[i].Attenuation);
                }

                // SpotLight
                for (int i = 0; i < L_Spot.Count; i++)
                {
                    IndexStr = i.ToString();
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightS[" + IndexStr + "].Pos"), L_Spot[i].Position);
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightS[" + IndexStr + "].Dir"), L_Spot[i].Direction);
                    GL.Uniform3(Shaders[ShaderIndex_P1].GetUniform("LightS[" + IndexStr + "].Int"), L_Spot[i].Color * L_Spot[i].Intensity);
                    GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LightS[" + IndexStr + "].Att"), L_Spot[i].Attenuation);
                    GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LightS[" + IndexStr + "].Cut"), MathHelper.DegreesToRadians(L_Spot[i].CutOFF));
                    GL.Uniform1(Shaders[ShaderIndex_P1].GetUniform("LightS[" + IndexStr + "].Exp"), L_Spot[i].Exponent);
                }
            }
            #endregion

            //Bind Textures
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Texture_Depth);
            for (int i = 0; i < Textures_P1.Length; i++)
            {
                GL.ActiveTexture(TextureUnit.Texture1 + i);
                GL.BindTexture(TextureTarget.Texture2D, Textures_P1[i]);
            }

            // BRDF_LUT Texture
            GL.ActiveTexture(TextureUnit.Texture1 + Textures_P1.Length);
            GL.BindTexture(TextureTarget.Texture2D, Texture_BRDF_LUT);

            // IBL Diffuse
            GL.ActiveTexture(TextureUnit.Texture2 + Textures_P1.Length);
            GL.BindTexture(TextureTarget.TextureCubeMap, Texture_IBL_Diff);

            // IBL Specular
            GL.ActiveTexture(TextureUnit.Texture3 + Textures_P1.Length);
            GL.BindTexture(TextureTarget.TextureCubeMap, Texture_IBL_Spec);

            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);

            Shaders[ShaderIndex_P1].EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.VertexAttribPointer(Shaders[ShaderIndex_P1].GetAttribute("v_UV"), 2, VertexAttribPointerType.Float, false, 0, 0);
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);

            //Shaders[ShaderIndex_P1].DisableVertexAttribArrays();
        }

        public static void Draw_PostPocess()
        {
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0); //Bind default FrameBuffer
            GL.Enable(EnableCap.FramebufferSrgb);
            GL.Disable(EnableCap.DepthTest);
            GL.ClearColor(Color4.Gray);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.UseProgram(Shaders[ShaderIndex_PP].ProgramID);

            if (UsePostEffects)
            {
                int TempLocation = 0;
                //TempLocation = Shaders[ShaderIndex_PP].GetUniform("ScreenSize");
                //if (TempLocation != -1)
                //    GL.Uniform2(TempLocation, (float)ScreenWidth, (float)ScreenHeight);

                //TempLocation = Shaders[ShaderIndex_PP].GetUniform("ClipPlanes");
                //if (TempLocation != -1) //zNear, zFar
                //    GL.Uniform2(TempLocation, Game.MainCamera.zNear, Game.MainCamera.zFar);

                TempLocation = Shaders[ShaderIndex_PP].GetUniform("FXAAEnabled");
                if (TempLocation != -1)
                {
                    GL.Uniform1(TempLocation, Convert.ToInt32(Settings.Graphics.FXAA.Enabled));

                    if (Settings.Graphics.FXAA.Enabled)
                        GL.Uniform3(Shaders[ShaderIndex_PP].GetUniform("FXAASettings"),
                            Settings.Graphics.FXAA.Subpix,
                            Settings.Graphics.FXAA.EdgeThreshold,
                            Settings.Graphics.FXAA.EdgeThresholdMin);
                }
            }

            //Bind Textures
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, Texture_PP);

            Shaders[ShaderIndex_PP].EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.VertexAttribPointer(Shaders[ShaderIndex_PP].GetAttribute("v_UV"), 2, VertexAttribPointerType.Float, false, 0, 0);
            GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);

            //Shaders[PP_ShaderIndex].DisableVertexAttribArrays();
        }

        public static void Free()
        {
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
            for (int i = 0; i < Shaders.Count; i++)
                Shaders[i].Free();

            ShaderIndex_P1 = 0;
            ShaderIndex_PP = 0;

            for (int i = 0; i < Textures_P1.Length; i++)
            {
                GL.DeleteTexture(Textures_P1[i]);
                Textures_P1[i] = 0;
            }

            GL.DeleteTexture(Texture_Depth);
            GL.DeleteTexture(Texture_PP);
            GL.DeleteTexture(Texture_BRDF_LUT);
            Texture_Depth = 0;
            Texture_PP = 0;
            Texture_BRDF_LUT = 0;

            if (VBO != 0)
            {
                GL.DeleteBuffer(VBO);
                VBO = 0;
            }

            if (FBO_P1 != 0)
            {
                GL.DeleteFramebuffer(FBO_P1);
                FBO_P1 = 0;
            }
            if (FBO_PP != 0)
            {
                GL.DeleteFramebuffer(FBO_PP);
                FBO_PP = 0;
            }
        }
    }
}