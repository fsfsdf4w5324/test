﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using TGASharpLib;
using HDRSharpLib;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using GDIPixelFormat = System.Drawing.Imaging.PixelFormat;
using Buffer = System.Buffer;

namespace LED_Engine_PBR
{
    public static class Textures
    {
        public static List<Texture> TEXTURES = new List<Texture>(); // Loaded Textures
        public static List<Texture> TexturesList = new List<Texture>(); // All Textures

        public static void LoadTexturesList(string XmlFile, string TexturePath, bool EngineContent)
        {
            try
            {
                XmlDocument XML = new XmlDocument();
                XmlNodeList xmlNodeList;

                #region Load Textures List
                XML.Load(XmlFile);
                xmlNodeList = XML.DocumentElement.SelectNodes("Texture");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    Texture texture = new Texture();
                    texture.EngineContent = EngineContent;
                    texture.TextureTarget = TextureTarget.Texture2D;
                    texture.WrapR = TextureWrapMode.Repeat;
                    texture.WrapS = TextureWrapMode.Repeat;
                    texture.WrapT = TextureWrapMode.Repeat;

                    texture.Name = xmlNode.SelectSingleNode("Name").InnerText;
                    texture.ImageFile = Engine.CombinePaths(TexturePath, xmlNode.SelectSingleNode("File").InnerText);

                    if (xmlNode.SelectNodes("Mipmaps").Count > 0)
                        texture.Mipmaps = Convert.ToBoolean(xmlNode.SelectSingleNode("Mipmaps").InnerText);

                    if (texture.Mipmaps)
                        texture.MinFilter = TextureMinFilter.LinearMipmapLinear;
                    else
                        texture.MinFilter = TextureMinFilter.Linear;

                    if (xmlNode.SelectNodes("MagFilter").Count > 0)
                    {
                        string StrMagFilter = xmlNode.SelectSingleNode("MagFilter").InnerText;
                        texture.MagFilter = (TextureMagFilter)Enum.Parse(typeof(TextureMagFilter), StrMagFilter, true);
                    }

                    if (xmlNode.SelectNodes("MinFilter").Count > 0)
                    {
                        string StrMinFilter = xmlNode.SelectSingleNode("MinFilter").InnerText;
                        texture.MinFilter = (TextureMinFilter)Enum.Parse(typeof(TextureMinFilter), StrMinFilter, true);
                    }

                    if (xmlNode.SelectNodes("sRGB").Count > 0)
                        texture.sRGB = Convert.ToBoolean(xmlNode.SelectSingleNode("sRGB").InnerText);

                    if (xmlNode.SelectNodes("AnisotropicFiltering").Count > 0)
                        texture.AnisotropicFiltering = Convert.ToBoolean(xmlNode.SelectSingleNode("AnisotropicFiltering").InnerText);

                    if (xmlNode.SelectNodes("TextureWrapR").Count > 0)
                    {
                        string StrTextureWrapMode = xmlNode.SelectSingleNode("TextureWrapR").InnerText;
                        texture.WrapR = (TextureWrapMode)Enum.Parse(typeof(TextureWrapMode), StrTextureWrapMode, true);
                    }

                    if (xmlNode.SelectNodes("TextureWrapS").Count > 0)
                    {
                        string StrTextureWrapMode = xmlNode.SelectSingleNode("TextureWrapS").InnerText;
                        texture.WrapS = (TextureWrapMode)Enum.Parse(typeof(TextureWrapMode), StrTextureWrapMode, true);
                    }

                    if (xmlNode.SelectNodes("TextureWrapT").Count > 0)
                    {
                        string StrTextureWrapMode = xmlNode.SelectSingleNode("TextureWrapT").InnerText;
                        texture.WrapT = (TextureWrapMode)Enum.Parse(typeof(TextureWrapMode), StrTextureWrapMode, true);
                    }

                    TexturesList.Add(texture);
                }
                #endregion
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Textures.LoadTexturesList() Exception.");
                Log.WriteLineRed("XmlFile: \"{0}\", EngineContent: \"{1}\"", XmlFile, EngineContent);
                Log.WriteLineYellow(e.Message);
            }
        }

        public static void LoadCubemapTexturesList(string XmlFile, string CubemapTexturePath, bool EngineContent)
        {
            try
            {
                XmlDocument XML = new XmlDocument();
                XmlNodeList xmlNodeList;

                #region Load CubemapTextures List
                XML.Load(XmlFile);
                xmlNodeList = XML.DocumentElement.SelectNodes("CubemapTexture");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    Texture texture = new Texture();
                    texture.EngineContent = EngineContent;
                    texture.TextureTarget = TextureTarget.TextureCubeMap;
                    texture.WrapR = TextureWrapMode.ClampToEdge;
                    texture.WrapS = TextureWrapMode.ClampToEdge;
                    texture.WrapT = TextureWrapMode.ClampToEdge;

                    string Name = xmlNode.SelectSingleNode("Name").InnerText;
                    texture.Name = Name;

                    string Dir = CubemapTexturePath;
                    if (xmlNode.SelectNodes("Dir").Count > 0)
                        Dir = Engine.CombinePaths(Dir, xmlNode.SelectSingleNode("Dir").InnerText);

                    XmlNodeList FileNode = xmlNode.SelectNodes("File");
                    if (FileNode.Count >= 6 || FileNode.Count == 1)
                    {
                        texture.CubemapFiles = new string[FileNode.Count];
                        for (int f = 0; f < FileNode.Count; f++)
                            texture.CubemapFiles[f] = Engine.CombinePaths(Dir, FileNode.Item(f).InnerText);
                    }
                    else
                        throw new Exception("Cubemap: \"" + Name + "\" Files count must be 6 or 1!!!");

                    if (xmlNode.SelectNodes("Mipmaps").Count > 0)
                        texture.Mipmaps = Convert.ToBoolean(xmlNode.SelectSingleNode("Mipmaps").InnerText);

                    if (texture.Mipmaps)
                        texture.MinFilter = TextureMinFilter.LinearMipmapLinear;
                    else
                        texture.MinFilter = TextureMinFilter.Linear;

                    if (xmlNode.SelectNodes("MagFilter").Count > 0)
                    {
                        string StrMagFilter = xmlNode.SelectSingleNode("MagFilter").InnerText;
                        texture.MagFilter = (TextureMagFilter)Enum.Parse(typeof(TextureMagFilter), StrMagFilter, true);
                    }

                    if (xmlNode.SelectNodes("MinFilter").Count > 0)
                    {
                        string StrMinFilter = xmlNode.SelectSingleNode("MinFilter").InnerText;
                        texture.MinFilter = (TextureMinFilter)Enum.Parse(typeof(TextureMinFilter), StrMinFilter, true);
                    }

                    if (xmlNode.SelectNodes("sRGB").Count > 0)
                        texture.sRGB = Convert.ToBoolean(xmlNode.SelectSingleNode("sRGB").InnerText);

                    if (xmlNode.SelectNodes("AnisotropicFiltering").Count > 0)
                        texture.AnisotropicFiltering = Convert.ToBoolean(xmlNode.SelectSingleNode("AnisotropicFiltering").InnerText);

                    if (xmlNode.SelectNodes("TextureWrapR").Count > 0)
                    {
                        string StrTextureWrapMode = xmlNode.SelectSingleNode("TextureWrapR").InnerText;
                        texture.WrapR = (TextureWrapMode)Enum.Parse(typeof(TextureWrapMode), StrTextureWrapMode, true);
                    }

                    if (xmlNode.SelectNodes("TextureWrapS").Count > 0)
                    {
                        string StrTextureWrapMode = xmlNode.SelectSingleNode("TextureWrapS").InnerText;
                        texture.WrapS = (TextureWrapMode)Enum.Parse(typeof(TextureWrapMode), StrTextureWrapMode, true);
                    }

                    if (xmlNode.SelectNodes("TextureWrapT").Count > 0)
                    {
                        string StrTextureWrapMode = xmlNode.SelectSingleNode("TextureWrapT").InnerText;
                        texture.WrapT = (TextureWrapMode)Enum.Parse(typeof(TextureWrapMode), StrTextureWrapMode, true);
                    }

                    TexturesList.Add(texture);
                }
                #endregion
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Textures.LoadCubemapTexturesList() Exception.");
                Log.WriteLineRed("XmlFile: \"{0}\", EngineContent: \"{1}\"", XmlFile, EngineContent);
                Log.WriteLineYellow(e.Message);
            }
        }

        public static Texture GetTexture(string Name)
        {
            foreach (var i in TEXTURES)
                if (i.Name.GetHashCode() == Name.GetHashCode())
                    return i;
            return null;
        }

        public static Texture GetTexture(int ID)
        {
            foreach (var i in TEXTURES)
                if (i.ID == ID)
                    return i;
            return null;
        }

        public static Texture Load(string Name)
        {
            Texture T = GetTexture(Name);

            if (T == null)
            {
                foreach (var i in TexturesList)
                    if (i.Name.GetHashCode() == Name.GetHashCode())
                        T = i;
                if (T == null)
                    return null;
            }

            if (T.UseCounter == 0)
            {
                switch (T.TextureTarget)
                {
                    case TextureTarget.TextureCubeMap:
                        T.Load_TextureCubemap();
                        break;
                    case TextureTarget.Texture2D:
                    default:
                        T.Load_Texture2D();
                        break;
                }
                TEXTURES.Add(T);
            }

            T.UseCounter++;
            return T;
        }

        public static void Unload(string Name)
        {
            Unload(GetTexture(Name));
        }

        public static void Unload(int ID)
        {
            Unload(GetTexture(ID));
        }

        public static void Unload(Texture T)
        {
            if (T != null)
            {
                if (T.UseCounter > 0)
                {
                    T.UseCounter--;

                    if (T.UseCounter == 0)
                    {
                        T.Free();
                        TEXTURES.Remove(T);
                    }
                }
            }
        }
    }

    public class Texture
    {
        public uint UseCounter = 0;
        public bool EngineContent = false;

        public int ID = 0;
        public TextureTarget TextureTarget = TextureTarget.Texture2D;
        public TextureMagFilter MagFilter = TextureMagFilter.Linear;
        public TextureMinFilter MinFilter = TextureMinFilter.LinearMipmapLinear;
        public bool sRGB = false;
        public bool Mipmaps = true;
        public bool AnisotropicFiltering = true;
        public string Name = String.Empty;
        public string ImageFile = String.Empty;
        public string[] CubemapFiles = null;
        public TextureWrapMode WrapR = TextureWrapMode.ClampToEdge;
        public TextureWrapMode WrapS = TextureWrapMode.ClampToEdge;
        public TextureWrapMode WrapT = TextureWrapMode.ClampToEdge;

        public void Load_Texture2D()
        {
            try
            {
                int Width;
                int Height;
                PixelFormat PixFormat;
                PixelType PixType;
                byte[] ImageData = Load_Texture_Bytes(ImageFile, out Width, out Height, out PixFormat, out PixType, true);
                Load_Texture2D(ImageData, Width, Height, PixFormat, PixType);
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Error: Cannot load Texture (\"{0}\") from File: \"{1}\".", Name, ImageFile);
                Log.WriteLineYellow(e.Message);
                Free();
            }
        }

        public void Load_Texture2D(byte[] ImageData, int Width, int Height, PixelFormat PixFormat = PixelFormat.Bgra, PixelType PixType = PixelType.UnsignedByte)
        {
            try
            {
                Free();

                ID = GL.GenTexture();
                GL.BindTexture(TextureTarget, ID);

                PixelInternalFormat Texture_PixelInternalFormat = sRGB ? PixelInternalFormat.SrgbAlpha : PixelInternalFormat.Rgba;

                GL.TexImage2D(TextureTarget, 0, Texture_PixelInternalFormat, Width, Height, 0, PixFormat, PixType, ImageData);

                //Filter parameters
                GL.TexParameter(TextureTarget, TextureParameterName.TextureMagFilter, (int)MagFilter);
                GL.TexParameter(TextureTarget, TextureParameterName.TextureMinFilter, (int)MinFilter);

                if (AnisotropicFiltering && Helper.IsExtensionSupported("GL_EXT_texture_filter_anisotropic"))
                {
                    int MaxAniso = GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt);
                    if (Settings.Graphics.AnisotropicFiltering != -1)
                        MaxAniso = Math.Min(MaxAniso, Settings.Graphics.AnisotropicFiltering);
                    GL.TexParameter(TextureTarget, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, MaxAniso);
                }

                GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapR, (int)WrapR);
                GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapS, (int)WrapS);
                GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapT, (int)WrapT);

                if (Mipmaps)
                    GL.GenerateMipmap((GenerateMipmapTarget)TextureTarget);
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Error: Cannot load texture. Name: \"{0}\".", Name);
                Log.WriteLineYellow(e.Message);
                Free();
            }
        }

        public void Load_TextureCubemap()
        {
            try
            {
                int Width = 0;
                int Height = 0;
                PixelFormat PixFormat = PixelFormat.Rgb;
                PixelType PixType = PixelType.UnsignedByte;

                if (CubemapFiles.Length == 6)
                {
                    byte[][] ImagesData = new byte[CubemapFiles.Length][];

                    for (int i = 0; i < CubemapFiles.Length; i++)
                    {
                        switch (i)
                        {
                            default:
                                break;
                            case 0:
                            case 1:
                            case 4:
                            case 5:
                                ImagesData[i] = Load_Texture_Bytes(CubemapFiles[i], out Width, out Height, out PixFormat, out PixType, true);
                                Array.Reverse(ImagesData[i]);

                                int BytesPerPixel = (int)(ImagesData[i].LongLength / (Width * Height));
                                for (long j = 0; j < ImagesData[i].LongLength; j += BytesPerPixel)
                                {
                                    for (int l = 0; l < BytesPerPixel / 2; l++)
                                    {
                                        long I1 = j + l;
                                        long I2 = j + (BytesPerPixel - l - 1);
                                        byte B1 = ImagesData[i][I1];
                                        byte B2 = ImagesData[i][I2];
                                        ImagesData[i][I1] = B2;
                                        ImagesData[i][I2] = B1;
                                    }
                                }
                                break;
                            case 2:
                            case 3:
                                ImagesData[i] = Load_Texture_Bytes(CubemapFiles[i], out Width, out Height, out PixFormat, out PixType, true);
                                break;
                        }
                    }

                    Load_TextureCubemap(ImagesData, Width, Height, PixFormat, PixType);
                }
                else if (CubemapFiles.Length == 1)
                {
                    Load_TextureCubemap(new byte[1][], Width, Height, PixFormat, PixType);
                }
                else
                    throw new Exception("CubemapTextures Files Count must be 6 or 1");
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Error: Cannot load CubemapTexture \"{0}\" from Files:", Name);
                for (int i = 0; i < CubemapFiles.Length; i++)
                    Log.WriteLine((ConsoleColor)(i + 1), "File{0}: \"{1}\"", i, CubemapFiles[i]);
                Log.WriteLineYellow(e.Message);
                Free();
            }
        }

        public void Load_TextureCubemap(byte[][] CubemapImageData, int Width, int Height, PixelFormat PixFormat = PixelFormat.Bgra, PixelType PixType = PixelType.UnsignedByte)
        {
            try
            {
                Free();

                ID = GL.GenTexture();
                GL.BindTexture(TextureTarget, ID);

                // Загружаем все текстуры граней
                PixelInternalFormat Texture_PixelInternalFormat = sRGB ? PixelInternalFormat.SrgbAlpha : PixelInternalFormat.Rgba;

                if (CubemapImageData.GetLength(0) == 6)
                {
                    TextureTarget[] targets = new TextureTarget[]
                    {
                        TextureTarget.TextureCubeMapPositiveX, TextureTarget.TextureCubeMapNegativeX,
                        TextureTarget.TextureCubeMapPositiveY, TextureTarget.TextureCubeMapNegativeY,
                        TextureTarget.TextureCubeMapPositiveZ, TextureTarget.TextureCubeMapNegativeZ
                    };

                    for (int i = 0; i < CubemapImageData.GetLength(0); i++)
                        GL.TexImage2D(targets[i], 0, Texture_PixelInternalFormat, Width, Height, 0, PixFormat, PixType, CubemapImageData[i]);
                }
                else if (CubemapImageData.GetLength(0) == 1)
                {
                    byte[] ImageData = Load_Texture_Bytes(CubemapFiles[0], out Width, out Height, out PixFormat, out PixType, true);

                    TextureTarget TexTarget = TextureTarget;
                    TextureTarget = TextureTarget.Texture2D;
                    Load_Texture2D(ImageData, Width, Height, PixFormat, PixType);
                    TextureTarget = TexTarget;

                    int CubeMapID = FBO.Generate_Cubemap_From_Equirectangular_Texture(ID, Height / 2);
                    GL.DeleteTexture(ID);
                    ID = CubeMapID;
                }
                else
                    throw new Exception("CubemapTextures Count must be 1 or 6!");

                //Filter parameters
                GL.TexParameter(TextureTarget, TextureParameterName.TextureMagFilter, (int)MagFilter);
                GL.TexParameter(TextureTarget, TextureParameterName.TextureMinFilter, (int)MinFilter);

                if (AnisotropicFiltering && Helper.IsExtensionSupported("GL_EXT_texture_filter_anisotropic"))
                {
                    int MaxAniso = GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt);
                    if (Settings.Graphics.AnisotropicFiltering != -1)
                        MaxAniso = Math.Min(MaxAniso, Settings.Graphics.AnisotropicFiltering);
                    GL.TexParameter(TextureTarget, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, MaxAniso);
                }

                GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapS, (int)WrapS);
                GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapT, (int)WrapT);
                GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapR, (int)WrapR);
                
                if (Mipmaps)
                    GL.GenerateMipmap((GenerateMipmapTarget)TextureTarget);
            }
            catch (Exception e)
            {
                Log.WriteLineRed("Error: Cannot load CubemapTexture. Name: \"{0}\".", Name);
                Log.WriteLineYellow(e.Message);
                Free();
            }
        }

        byte[] Load_Texture_Bytes(string ImageFile, out int Width, out int Height, out PixelFormat PixFormat, out PixelType PixType, bool FlipY = true)
        {
            if (!File.Exists(ImageFile))
                throw new FileNotFoundException(ImageFile);

            string FileExtension = Path.GetExtension(ImageFile).TrimStart(new char[] { '.' }).ToLowerInvariant();
            if (FileExtension == "hdr")
            {
                HDR Hdr = HDR.FromFile(ImageFile);

                Width = Hdr.Width;
                Height = Hdr.Height;
                PixFormat = PixelFormat.Rgb;
                PixType = PixelType.Float;

                int ScanlineSizeBytes = Width * sizeof(float) * 3;
                byte[] ResultBytes = new byte[Height * ScanlineSizeBytes];

                for (int i = 0; i < Height; i++) // Copy and Flip Y
                {
                    int DestOffset = (FlipY ? ResultBytes.Length - i * ScanlineSizeBytes - ScanlineSizeBytes : i * ScanlineSizeBytes);
                    Buffer.BlockCopy(Hdr.ImageData, i * ScanlineSizeBytes, ResultBytes, DestOffset, ScanlineSizeBytes);
                }
                return ResultBytes;
            }
            else
            {
                Bitmap BMP;
                switch (FileExtension)
                {
                    default:
                    case null:
                    case "":
                        BMP = (Bitmap)Image.FromFile(ImageFile);
                        break;

                    case "tga":
                    case "targa":
                        BMP = TGA.FromFile(ImageFile).ToBitmap();
                        break;

                    case "dds":
                        BMP = DDSReaderSharp.ToBitmap(File.ReadAllBytes(ImageFile));
                        break;
                }
                GDIPixelFormat BmpPixelFormat = BMP.PixelFormat;

                Width = BMP.Width;
                Height = BMP.Height;
                PixType = PixelType.UnsignedByte;

                switch (BmpPixelFormat)
                {
                    default:
                    case GDIPixelFormat.Format32bppArgb:
                    case GDIPixelFormat.Format32bppPArgb:
                    case GDIPixelFormat.Format32bppRgb:
                        PixFormat = PixelFormat.Bgra;
                        break;
                    case GDIPixelFormat.Format24bppRgb:
                        PixFormat = PixelFormat.Bgr;
                        break;
                    case GDIPixelFormat.Alpha:
                    case GDIPixelFormat.PAlpha:
                        PixFormat = PixelFormat.Alpha;
                        break;
                    case GDIPixelFormat.Format8bppIndexed:
                        PixFormat = PixelFormat.Red;
                        break;
                }

                // Bitmap width must by aligned (align value = 32 bits = 4 bytes)!
                int StrideBytes = BMP.Width * Image.GetPixelFormatSize(BmpPixelFormat) >> 3;
                int PaddingBytes = (int)Math.Ceiling(StrideBytes / 4.0) * 4 - StrideBytes;

                byte[] ImgData = new byte[(StrideBytes + PaddingBytes) * BMP.Height];
                BitmapData BmpData = BMP.LockBits(new Rectangle(0, 0, BMP.Width, BMP.Height), ImageLockMode.ReadOnly, BmpPixelFormat);
                Marshal.Copy(BmpData.Scan0, ImgData, 0, ImgData.Length);
                BMP.UnlockBits(BmpData);
                BmpData = null;

                byte[] ResultBytes = new byte[StrideBytes * Height];

                for (int i = 0; i < Height; i++) // Copy and flip Y
                {
                    long DestOffset = (FlipY ? ResultBytes.LongLength - i * StrideBytes - StrideBytes : i * StrideBytes);
                    Array.Copy(ImgData, i * (StrideBytes + PaddingBytes), ResultBytes, DestOffset, StrideBytes);
                }

                return ResultBytes;
            }
        }

        public void Free()
        {
            GL.DeleteTexture(ID);
            ID = 0;

            UseCounter = 0;
        }
    }
}