﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace LED_Engine_PBR
{
    partial class Game
    {
        //public Game() : base(800, 600, GraphicsMode.Default, "LED Engine PBR", GameWindowFlags.Default, DisplayDevice.Default, 4, 6,
        //    GraphicsContextFlags.Default | GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug)
        //{
        //    VSync = VSyncMode.On;
        //}

        protected override void OnLoad(EventArgs e)
        {
            Icon = Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location);
            Engine.LoadConfig();

            WindowApplySettings(this);
            Engine.GetGLSettings();
            Engine.LoadContentLists();
            FBO.Init(Width, Height);
            Engine.LoadEngineContent();
            Maps.LoadMap(Args.Length > 0 ? Args[0] : "SampleMap"); //Sponza //SampleMap //Materials_Test_Map
            FBO.Generate_PBR_Textures();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Maps.Free(true); // Free all: Map -> Meshes, Shaders, Textures...
            FBO.Free();
            FPS.Font_Free();
            Engine.ClearLists();

            //Console.ReadKey();
            if (Log.HasErrors)
                Clipboard.SetText(Log.GetLog());
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            string[] DrawModes = Enum.GetNames(typeof(DrawMode));

            switch (e.Key)
            {
                case Key.Escape:
                    Close();
                    break;

                case Key.F1:
                    if (Settings.DrawMode.ToString() == DrawModes[0])
                        Settings.DrawMode = (DrawMode)Enum.Parse(typeof(DrawMode), DrawModes[DrawModes.Length - 1]);
                    else
                        Settings.DrawMode = (DrawMode)((int)(Settings.DrawMode) - 1);
                    break;

                case Key.F2:
                    if (Settings.DrawMode.ToString() == DrawModes[DrawModes.Length - 1])
                        Settings.DrawMode = (DrawMode)Enum.Parse(typeof(DrawMode), DrawModes[0]);
                    else
                        Settings.DrawMode = (DrawMode)((int)(Settings.DrawMode) + 1);
                    break;

                case Key.F12:
                    LightSettings lightSettings = new LightSettings();
                    lightSettings.Show();
                    break;

                case Key.F3:
                    GL.Enable(EnableCap.DebugOutput);
                    GL.Enable(EnableCap.DebugOutputSynchronous);
                    GL.DebugMessageCallback(DebugCallback, IntPtr.Zero);
                    int IDs = 0;
                    GL.DebugMessageControl(DebugSourceControl.DontCare, DebugTypeControl.DontCare, DebugSeverityControl.DontCare, 0, ref IDs, true);
                    break;
            }

            #region DrawMode
            switch (Settings.DrawMode)
            {
                case DrawMode.Default:
                    FBO.ShaderIndex_P1 = FBO.ShaderIndex_P1_Orig;
                    break;
                case DrawMode.Depth:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Depth;
                    break;
                case DrawMode.BaseColor:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_BaseColor;
                    break;
                case DrawMode.Normals:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Normals;
                    break;
                case DrawMode.Roughness:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Roughness;
                    break;
                case DrawMode.Metalness:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Metalness;
                    break;
                case DrawMode.Emissive:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Emissive;
                    break;
                case DrawMode.AmbientOclusion:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_AmbientOcclusion;
                    break;
                case DrawMode.Height:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Height;
                    break;
                case DrawMode.Light:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_Light;
                    break;
                case DrawMode.BRDF_LUT:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_BRDF_LUT;
                    break;
                case DrawMode.IBL_Diffuse:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_IBL_Diffuse;
                    break;
                case DrawMode.IBL_Specular:
                    FBO.ShaderIndex_P1 = FBO.DebugShaderIndex_IBL_Specular;
                    break;
            }
            #endregion
        }

        private void DebugCallback(DebugSource source, DebugType type, int id, DebugSeverity severity, int length, IntPtr message, IntPtr userParam)
        {
            Log.WriteLine("---------------------opengl-callback-start------------");
            Log.WriteLine("Message: {0}", message);
            Log.WriteLine("Type: {0}", type.ToString());
            Log.WriteLine("ID: {0}", id);
            Log.WriteLine("Severity: {0}", severity.ToString());
            Log.WriteLine("---------------------opengl-callback-end--------------");
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            if (e.Mouse.IsButtonDown(MouseButton.Left))
            {
                float Mouse_Sensivity = 0.003f;
                MainCamera.AddRotation(-e.XDelta * Mouse_Sensivity, -e.YDelta * Mouse_Sensivity);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            if (Width > 0 && Height > 0)
            {
                Settings.Window.Width = Width;
                Settings.Window.Height = Height;

                MainCamera.SetProjectionMatrix(ProjectionTypes.Perspective, Width, Height, MainCamera.zNear, MainCamera.zFar, MainCamera.FOV);
                GL.Viewport(0, 0, Width, Height);
                FBO.Rescale(Width, Height);
                FPS.Font_Init();
                FPS.FPS_Font_ProjectionMatrix = Matrix4.CreateOrthographicOffCenter(0, Width, 0, Height, -1.0f, 1.0f);
            }
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            if (!Focused)
                return;

            KeybrdState = Keyboard.GetState();
            bool AcselerateInput = KeybrdState[Key.ShiftLeft];

            float camMoveSens = AcselerateInput ? 1f : 0.2f;
            float camRotateSens = AcselerateInput ? 0.1f : 0.02f;
            float MoveX = 0.0f, MoveY = 0.0f, MoveZ = 0.0f,
                RotateX = 0.0f, RotateY = 0.0f;

            if (KeybrdState[Key.W])
                MoveY += camMoveSens;
            if (KeybrdState[Key.A])
                MoveX -= camMoveSens;
            if (KeybrdState[Key.S])
                MoveY -= camMoveSens;
            if (KeybrdState[Key.D])
                MoveX += camMoveSens;
            if (KeybrdState[Key.E])
                MoveZ += camMoveSens;
            if (KeybrdState[Key.Q])
                MoveZ -= camMoveSens;

            if (MoveX != 0 || MoveY != 0 || MoveZ != 0)
                MainCamera.Move(MoveX, MoveY, MoveZ);

            if (KeybrdState[Key.Left])
                RotateX += camRotateSens;
            if (KeybrdState[Key.Right])
                RotateX -= camRotateSens;
            if (KeybrdState[Key.Up])
                RotateY += camRotateSens;
            if (KeybrdState[Key.Down])
                RotateY -= camRotateSens;

            if (RotateX != 0 || RotateY != 0)
                MainCamera.AddRotation(RotateX, RotateY);

            if (SkyBox != null)
                SkyBox.Position = MainCamera.Position;
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            // G-Buffer
            GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBO.FBO_P1);
            GL.Enable(EnableCap.DepthTest); // Включаем тест глубины
            GL.DepthFunc(DepthFunction.Lequal);
            GL.ClearColor(Color4.Gray);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Copy All Meshes to DrawableObjects
            List<Mesh> DrawableMeshes = new List<Mesh>();

            for (int i = 0; i < Models.MODELS.Count; i++)
                if (Models.MODELS[i].Visible)
                    DrawableMeshes.AddRange(Models.MODELS[i].Meshes);

            foreach (Mesh v in DrawableMeshes) //Draw All
                Draw(v);

            // Draw All Geometry to PostProcess FBO
            FBO.Draw_P1();

            // Draw SkyBox to PostProcess FBO
            if (SkyBox != null)
            {
                GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBO.FBO_PP);
                GL.ClearColor(Color4.Gray);
                GL.Enable(EnableCap.CullFace);
                GL.Enable(EnableCap.DepthTest);
                GL.Enable(EnableCap.TextureCubeMapSeamless);
                Draw(SkyBox);
            }

            // Draw PostProcessed Result to Screen (FBO = 0)
            FBO.Draw_PostPocess();

            if (FPS.ShowFPS)
                FPS.CalcFPS(e.Time); //FPS Counter, must be at the END of the Render LOOP!

            SwapBuffers(); // Swap the front and back buffer, displaying the scene
        }
    }
}